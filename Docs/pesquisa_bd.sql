-- phpMyAdmin SQL Dump
-- version 3.4.5deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 01/11/2011 às 15h26min
-- Versão do Servidor: 5.1.58
-- Versão do PHP: 5.3.6-13ubuntu3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `pesquisa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `alt_alternativa`
--

CREATE TABLE IF NOT EXISTS `alt_alternativa` (
  `alt_id` int(11) NOT NULL AUTO_INCREMENT,
  `alt_descricao` text,
  `per_id` int(11) NOT NULL,
  `ima_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`alt_id`,`per_id`,`ima_id`),
  KEY `fk_alt_alternativa_per_pergunta` (`per_id`),
  KEY `fk_alt_alternativa_ima_imagem1` (`ima_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `alt_alternativa`
--

INSERT INTO `alt_alternativa` (`alt_id`, `alt_descricao`, `per_id`, `ima_id`) VALUES
(1, 'Sim', 1, 0),
(2, 'NÃ£o', 1, 0),
(3, 'Boa', 2, 0),
(4, 'Ruim', 2, 0),
(5, 'PÃ©ssima', 2, 0),
(6, 'sim', 4, 0),
(7, 'nao', 4, 0),
(8, 'sim', 5, 0),
(9, 'nao', 5, 0),
(10, 'com certeza', 5, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ape_acessopesquisa`
--

CREATE TABLE IF NOT EXISTS `ape_acessopesquisa` (
  `ape_id` int(11) NOT NULL,
  `ape_descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ape_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ape_acessopesquisa`
--

INSERT INTO `ape_acessopesquisa` (`ape_id`, `ape_descricao`) VALUES
(1, 'aberto'),
(2, 'protegido');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cac_codigoacesso`
--

CREATE TABLE IF NOT EXISTS `cac_codigoacesso` (
  `cac_id` int(11) NOT NULL AUTO_INCREMENT,
  `cac_codigo` varchar(45) DEFAULT NULL,
  `pes_id` int(11) NOT NULL,
  `peq_id` int(11) NOT NULL,
  PRIMARY KEY (`cac_id`,`pes_id`,`peq_id`),
  KEY `fk_cac_codigoacesso_poa_pessoa1` (`pes_id`),
  KEY `fk_cac_codigoacesso_pes_pesquisa1` (`peq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `con_convite`
--

CREATE TABLE IF NOT EXISTS `con_convite` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT,
  `con_status` tinyint(1) DEFAULT NULL,
  `con_data` datetime DEFAULT NULL,
  `usu_id` int(11) NOT NULL,
  `pes_id` int(11) NOT NULL,
  PRIMARY KEY (`con_id`,`usu_id`,`pes_id`),
  KEY `fk_con_convite_usu_usuario1` (`usu_id`),
  KEY `fk_con_convite_pes_pessoa1` (`pes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ctr_contrato`
--

CREATE TABLE IF NOT EXISTS `ctr_contrato` (
  `ctr_id` int(11) NOT NULL AUTO_INCREMENT,
  `ctr_iniciovigencia` datetime DEFAULT NULL,
  `ctr_fimvigencia` datetime DEFAULT NULL,
  `pla_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  PRIMARY KEY (`ctr_id`,`pla_id`),
  KEY `fk_ctr_contrato_pla_plano1` (`pla_id`),
  KEY `fk_ctr_contrato_emp_empresa1` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dis_dissertativa`
--

CREATE TABLE IF NOT EXISTS `dis_dissertativa` (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_descricao` text,
  `res_id` int(11) NOT NULL,
  PRIMARY KEY (`dis_id`,`res_id`),
  KEY `fk_dis_dissertativa_res_resposta1` (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `end_endereco`
--

CREATE TABLE IF NOT EXISTS `end_endereco` (
  `end_id` int(11) NOT NULL AUTO_INCREMENT,
  `end_logradouro` varchar(100) DEFAULT NULL,
  `end_bairro` varchar(100) DEFAULT NULL,
  `end_numero` int(11) DEFAULT NULL,
  `end_estado` varchar(2) DEFAULT NULL,
  `end_cidade` varchar(100) DEFAULT NULL,
  `end_cep` int(11) DEFAULT NULL,
  `end_iniciovigencia` datetime DEFAULT NULL,
  `end_fimvigencia` datetime DEFAULT NULL,
  `end_sequencia` int(11) DEFAULT NULL,
  `usu_id` int(11) NOT NULL,
  PRIMARY KEY (`end_id`),
  KEY `fk_end_endereco_usu_usuario1` (`usu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `for_formulario`
--

CREATE TABLE IF NOT EXISTS `for_formulario` (
  `for_id` int(11) NOT NULL AUTO_INCREMENT,
  `for_nome` varchar(45) DEFAULT NULL,
  `for_descricao` text,
  `usu_id` int(11) NOT NULL,
  `tem_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`for_id`,`usu_id`,`tem_id`),
  KEY `fk_for_formulario_usu_usuario1` (`usu_id`),
  KEY `fk_for_formulario_tem_tema1` (`tem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `for_formulario`
--

INSERT INTO `for_formulario` (`for_id`, `for_nome`, `for_descricao`, `usu_id`, `tem_id`) VALUES
(1, 'Sala de aul', 'Sobre a sala de aula', 2, 1),
(2, 'SatisfaÃ§Ã£o de alunos', 'Pesquisa de satisfaÃ§Ã£o de alunos de uma escola, universidade', 1, 1),
(3, 'formulario da sala', 'sala', 2, 1),
(4, 'formulario da sala', 'sala', 2, 1),
(5, 'formulario do admin2', 'xzccererexcwe', 1, 1),
(6, 'aeearr', 'ffegbfvvs', 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `gpo_grupopessoa`
--

CREATE TABLE IF NOT EXISTS `gpo_grupopessoa` (
  `gpo_id` int(11) NOT NULL AUTO_INCREMENT,
  `pes_id` int(11) NOT NULL,
  `gru_id` int(11) NOT NULL,
  PRIMARY KEY (`gpo_id`),
  KEY `fk_gpo_grupopessoa_poa_pessoa1` (`pes_id`),
  KEY `fk_gpo_grupopessoa_gru_grupo1` (`gru_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `gru_grupo`
--

CREATE TABLE IF NOT EXISTS `gru_grupo` (
  `gru_id` int(11) NOT NULL AUTO_INCREMENT,
  `gru_nome` varchar(100) DEFAULT NULL,
  `usu_usuario_usu_id` int(11) NOT NULL,
  `tgr_id` int(11) NOT NULL,
  PRIMARY KEY (`gru_id`,`tgr_id`),
  KEY `fk_gru_grupo_usu_usuario1` (`usu_usuario_usu_id`),
  KEY `fk_gru_grupo_tgr_tipogrupo1` (`tgr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ima_imagem`
--

CREATE TABLE IF NOT EXISTS `ima_imagem` (
  `ima_id` int(11) NOT NULL AUTO_INCREMENT,
  `ima_nome` varchar(255) DEFAULT NULL,
  `ima_descricao` text,
  `ima_criacao` date DEFAULT NULL,
  `usu_id` int(11) NOT NULL,
  PRIMARY KEY (`ima_id`,`usu_id`),
  KEY `fk_ima_imagem_usu_usuario1` (`usu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `ima_imagem`
--

INSERT INTO `ima_imagem` (`ima_id`, `ima_nome`, `ima_descricao`, `ima_criacao`, `usu_id`) VALUES
(0, '0', '0', NULL, 1),
(1, 'default', 'default', '2011-08-22', 1),
(2, 'avatar_op7.JPG', 'Imagem de plano', '2011-08-22', 1),
(3, 'avatar_op7.JPG', 'Imagem de plano', '2011-08-22', 1),
(4, NULL, 'Imagem de plano', '2011-09-23', 1),
(5, NULL, 'Imagem de plano', '2011-09-23', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `irs_identificacaoredesocial`
--

CREATE TABLE IF NOT EXISTS `irs_identificacaoredesocial` (
  `irs_id` int(11) NOT NULL AUTO_INCREMENT,
  `irs_identificacao` varchar(150) DEFAULT NULL,
  `rso_id` int(11) NOT NULL,
  `pes_id` int(11) NOT NULL,
  PRIMARY KEY (`irs_id`,`rso_id`,`pes_id`),
  KEY `fk_irs_identificacaoredesocial_rso_redesocial1` (`rso_id`),
  KEY `fk_irs_identificacaoredesocial_poa_pessoa1` (`pes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lpa_limiteplano`
--

CREATE TABLE IF NOT EXISTS `lpa_limiteplano` (
  `lpa_id` int(11) NOT NULL AUTO_INCREMENT,
  `lpa_valor` int(11) DEFAULT NULL,
  `tli_id` int(11) NOT NULL,
  `pla_id` int(11) NOT NULL,
  PRIMARY KEY (`lpa_id`,`tli_id`,`pla_id`),
  KEY `fk_lpa_limiteplano_tli_tipolimite1` (`tli_id`),
  KEY `fk_lpa_limiteplano_pla_plano1` (`pla_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lpe_limitepesquisa`
--

CREATE TABLE IF NOT EXISTS `lpe_limitepesquisa` (
  `lpe_id` int(11) NOT NULL AUTO_INCREMENT,
  `lpe_limite` int(11) DEFAULT NULL,
  `peq_id` int(11) NOT NULL,
  PRIMARY KEY (`lpe_id`),
  KEY `fk_lpe_limitepesquisa_pes_pesquisa1` (`peq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mem_membro`
--

CREATE TABLE IF NOT EXISTS `mem_membro` (
  `mem_id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_iniciovigencia` datetime DEFAULT NULL,
  `mem_fimvigencia` datetime DEFAULT NULL,
  `usu_id` int(11) NOT NULL,
  `pco_id` int(11) NOT NULL,
  PRIMARY KEY (`mem_id`),
  KEY `fk_mem_membro_usu_usuario1` (`usu_id`),
  KEY `fk_mem_membro_pco_permissaocontrato1` (`pco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `num_numerico`
--

CREATE TABLE IF NOT EXISTS `num_numerico` (
  `num_id` int(11) NOT NULL AUTO_INCREMENT,
  `num_resposta` int(11) DEFAULT NULL,
  `res_id` int(11) NOT NULL,
  PRIMARY KEY (`num_id`,`res_id`),
  KEY `fk_num_numerico_res_resposta1` (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pag_pagamento`
--

CREATE TABLE IF NOT EXISTS `pag_pagamento` (
  `pag_id` int(11) NOT NULL AUTO_INCREMENT,
  `pag_datapagamento` date DEFAULT NULL,
  `pag_vencimento` date DEFAULT NULL,
  `pag_valorcobrado` float DEFAULT NULL,
  `pag_valorpago` float DEFAULT NULL,
  `pag_juros` float DEFAULT NULL,
  `ctr_id` int(11) NOT NULL,
  PRIMARY KEY (`pag_id`,`ctr_id`),
  KEY `fk_pag_pagamento_ctr_contrato1` (`ctr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `par_participantes`
--

CREATE TABLE IF NOT EXISTS `par_participantes` (
  `par_id` int(11) NOT NULL AUTO_INCREMENT,
  `peq_id` int(11) NOT NULL,
  `pes_id` int(11) NOT NULL,
  PRIMARY KEY (`par_id`),
  KEY `fk_par_participantes_pes_pesquisa1` (`peq_id`),
  KEY `fk_par_participantes_poa_pessoa1` (`pes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pco_permissaocontrato`
--

CREATE TABLE IF NOT EXISTS `pco_permissaocontrato` (
  `pco_id` int(11) NOT NULL AUTO_INCREMENT,
  `pco_nome` varchar(45) DEFAULT NULL,
  `pco_descricao` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`pco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `peq_pesquisa`
--

CREATE TABLE IF NOT EXISTS `peq_pesquisa` (
  `peq_id` int(11) NOT NULL AUTO_INCREMENT,
  `peq_nome` varchar(45) DEFAULT NULL,
  `peq_descricao` text,
  `peq_datainicio` datetime DEFAULT NULL,
  `peq_datafim` datetime DEFAULT NULL,
  `peq_url` varchar(255) DEFAULT NULL,
  `peq_senha` varchar(20) DEFAULT NULL,
  `for_id` int(11) NOT NULL,
  `ape_id` int(11) NOT NULL,
  PRIMARY KEY (`peq_id`,`for_id`,`ape_id`),
  KEY `fk_pes_pesquisa_for_formulario1` (`for_id`),
  KEY `fk_pes_pesquisa_ape_acessopesquisa1` (`ape_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `peq_pesquisa`
--

INSERT INTO `peq_pesquisa` (`peq_id`, `peq_nome`, `peq_descricao`, `peq_datainicio`, `peq_datafim`, `peq_url`, `peq_senha`, `for_id`, `ape_id`) VALUES
(1, 'Escolar', 'SSD', NULL, NULL, '/resposta/questionario/peq/ 1 ', NULL, 1, 1),
(2, 'Aula de JAVA', 'Sobre a sala de aula em java', NULL, NULL, '/resposta/questionario/peq/ 2 ', NULL, 3, 2),
(3, 'pesquisa do select', 'funciona', NULL, NULL, '/resposta/questionario/peq/ 3 ', NULL, 4, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `per_pergunta`
--

CREATE TABLE IF NOT EXISTS `per_pergunta` (
  `per_id` int(11) NOT NULL AUTO_INCREMENT,
  `per_descricao` text,
  `tpe_id` int(11) NOT NULL,
  `for_id` int(11) NOT NULL DEFAULT '0',
  `per_predefinida` tinyint(1) DEFAULT '0',
  `ima_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`per_id`,`for_id`),
  KEY `fk_per_pergunta_tpe_tipopergunta1` (`tpe_id`),
  KEY `fk_per_pergunta_for_formulario1` (`for_id`),
  KEY `fk_per_pergunta_ima_imagem1` (`ima_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `per_pergunta`
--

INSERT INTO `per_pergunta` (`per_id`, `per_descricao`, `tpe_id`, `for_id`, `per_predefinida`, `ima_id`) VALUES
(1, 'VocÃª estuda em nossa instituiÃ§Ã£o?', 1, 2, 0, NULL),
(2, 'O que vocÃª acha da sala de aula?', 1, 1, 0, NULL),
(3, 'essa pergunta', 2, 4, 0, NULL),
(4, 'vai ser assim?', 1, 4, 0, NULL),
(5, 'campeoa brasileiro', 1, 5, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pes_pessoa`
--

CREATE TABLE IF NOT EXISTS `pes_pessoa` (
  `pes_id` int(11) NOT NULL AUTO_INCREMENT,
  `pes_nome` varchar(150) DEFAULT NULL,
  `pes_email` varchar(255) DEFAULT NULL,
  `pes_datanascimento` date DEFAULT NULL,
  `tpo_id` int(11) NOT NULL,
  PRIMARY KEY (`pes_id`),
  KEY `fk_poa_pessoa_tpo_tipopessoa1` (`tpo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `pes_pessoa`
--

INSERT INTO `pes_pessoa` (`pes_id`, `pes_nome`, `pes_email`, `pes_datanascimento`, `tpo_id`) VALUES
(1, 'administrador', 'ronaldo@season.com.br', '2011-08-22', 1),
(2, 'usuario', 'usuario@gmail.com', '2011-02-10', 1),
(3, 'santos', 'santos@santos.com.br', '2010-02-10', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pgr_pesquisagrupo`
--

CREATE TABLE IF NOT EXISTS `pgr_pesquisagrupo` (
  `pgr_id` int(11) NOT NULL AUTO_INCREMENT,
  `gru_id` int(11) NOT NULL,
  `peq_id` int(11) NOT NULL,
  PRIMARY KEY (`pgr_id`,`gru_id`,`peq_id`),
  KEY `fk_pgr_pesquisagrupo_gru_grupo1` (`gru_id`),
  KEY `fk_pgr_pesquisagrupo_peq_pesquisa1` (`peq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pla_plano`
--

CREATE TABLE IF NOT EXISTS `pla_plano` (
  `pla_id` int(11) NOT NULL AUTO_INCREMENT,
  `pla_nome` varchar(45) DEFAULT NULL,
  `pla_valor` float DEFAULT NULL,
  `pla_descricao` text,
  `pla_status` tinyint(1) DEFAULT NULL,
  `ima_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pla_id`,`ima_id`),
  KEY `fk_pla_plano_ima_imagem1` (`ima_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `pla_plano`
--

INSERT INTO `pla_plano` (`pla_id`, `pla_nome`, `pla_valor`, `pla_descricao`, `pla_status`, `ima_id`) VALUES
(1, 'rooonaldo', 1002, 'Plano ronaldo', 2, 3),
(2, 'teste', 1234, 'pele', 1, 4),
(3, 'edson', 2324, 'teette', 1, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `res_resposta`
--

CREATE TABLE IF NOT EXISTS `res_resposta` (
  `res_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_respostadissertativa` varchar(100) DEFAULT NULL,
  `alt_id` int(11) DEFAULT NULL,
  `per_id` int(11) NOT NULL,
  `peq_id` int(11) NOT NULL,
  `pes_id` int(11) NOT NULL,
  PRIMARY KEY (`res_id`,`per_id`,`pes_id`),
  KEY `fk_res_resposta_alt_alternativa1` (`alt_id`),
  KEY `fk_res_resposta_per_pergunta1` (`per_id`),
  KEY `fk_res_resposta_pes_pesquisa1` (`peq_id`),
  KEY `fk_res_resposta_poa_pessoa1` (`pes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `res_resposta`
--

INSERT INTO `res_resposta` (`res_id`, `res_respostadissertativa`, `alt_id`, `per_id`, `peq_id`, `pes_id`) VALUES
(1, NULL, 4, 2, 1, 2),
(2, NULL, 6, 4, 3, 2),
(3, 'oq tem ela?', NULL, 3, 3, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `rso_redesocial`
--

CREATE TABLE IF NOT EXISTS `rso_redesocial` (
  `rso_id` int(11) NOT NULL AUTO_INCREMENT,
  `rso_nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`rso_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sus_suspensao`
--

CREATE TABLE IF NOT EXISTS `sus_suspensao` (
  `sus_id` int(11) NOT NULL AUTO_INCREMENT,
  `sus_datasuspensao` datetime DEFAULT NULL,
  `sus_datareativacao` datetime DEFAULT NULL,
  `ctr_id` int(11) NOT NULL,
  PRIMARY KEY (`sus_id`,`ctr_id`),
  KEY `fk_sus_suspensao_ctr_contrato1` (`ctr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tel_telefone`
--

CREATE TABLE IF NOT EXISTS `tel_telefone` (
  `tel_id` int(11) NOT NULL AUTO_INCREMENT,
  `tel_numero` int(10) DEFAULT NULL,
  `tel_iniciovigencia` datetime DEFAULT NULL,
  `tel_fimvigencia` datetime DEFAULT NULL,
  `usu_id` int(11) NOT NULL,
  PRIMARY KEY (`tel_id`),
  KEY `fk_tel_telefone_usu_usuario1` (`usu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tem_tema`
--

CREATE TABLE IF NOT EXISTS `tem_tema` (
  `tem_id` int(11) NOT NULL AUTO_INCREMENT,
  `tem_nomereal` varchar(45) DEFAULT NULL,
  `tem_nomefantasia` varchar(45) DEFAULT NULL,
  `ima_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tem_id`,`ima_id`),
  KEY `fk_tem_tema_ima_imagem1` (`ima_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `tem_tema`
--

INSERT INTO `tem_tema` (`tem_id`, `tem_nomereal`, `tem_nomefantasia`, `ima_id`) VALUES
(1, 'cupertino', 'default', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tgr_tipogrupo`
--

CREATE TABLE IF NOT EXISTS `tgr_tipogrupo` (
  `tgr_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgr_nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tgr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tli_tipolimite`
--

CREATE TABLE IF NOT EXISTS `tli_tipolimite` (
  `tli_id` int(11) NOT NULL AUTO_INCREMENT,
  `tli_descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tli_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tpe_tipopergunta`
--

CREATE TABLE IF NOT EXISTS `tpe_tipopergunta` (
  `tpe_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpe_descricao` varchar(100) DEFAULT NULL,
  `tpe_nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tpe_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tpe_tipopergunta`
--

INSERT INTO `tpe_tipopergunta` (`tpe_id`, `tpe_descricao`, `tpe_nome`) VALUES
(1, 'dissertavida', 'dissertavida'),
(2, 'alternativa', 'alternativa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tpo_tipopessoa`
--

CREATE TABLE IF NOT EXISTS `tpo_tipopessoa` (
  `tpo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpo_descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tpo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tpo_tipopessoa`
--

INSERT INTO `tpo_tipopessoa` (`tpo_id`, `tpo_descricao`) VALUES
(1, 'Usuario'),
(2, 'Visitante');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tus_tipousuario`
--

CREATE TABLE IF NOT EXISTS `tus_tipousuario` (
  `tus_id` int(11) NOT NULL AUTO_INCREMENT,
  `tus_descricao` varchar(100) DEFAULT NULL,
  `tus_nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tus_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tus_tipousuario`
--

INSERT INTO `tus_tipousuario` (`tus_id`, `tus_descricao`, `tus_nome`) VALUES
(1, 'Proprietario', 'Proprietario'),
(2, 'Membro', 'Membro'),
(3, 'Administrador do sistema', 'admin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usu_usuario`
--

CREATE TABLE IF NOT EXISTS `usu_usuario` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_usuario` varchar(25) DEFAULT NULL,
  `usu_senha` varchar(25) DEFAULT NULL,
  `pes_id` int(11) NOT NULL,
  `tus_id` int(11) NOT NULL,
  PRIMARY KEY (`usu_id`),
  KEY `fk_usu_usuario_poa_pessoa1` (`pes_id`),
  KEY `fk_usu_usuario_tus_tipousuario1` (`tus_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `usu_usuario`
--

INSERT INTO `usu_usuario` (`usu_id`, `usu_usuario`, `usu_senha`, `pes_id`, `tus_id`) VALUES
(1, 'admin', 'admin', 1, 3),
(2, 'usuario', 'usuario', 2, 1),
(3, 'santos', 'santos', 3, 1);

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `alt_alternativa`
--
ALTER TABLE `alt_alternativa`
  ADD CONSTRAINT `alt_alternativa_ibfk_1` FOREIGN KEY (`per_id`) REFERENCES `per_pergunta` (`per_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_alt_alternativa_ima_imagem1` FOREIGN KEY (`ima_id`) REFERENCES `ima_imagem` (`ima_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `cac_codigoacesso`
--
ALTER TABLE `cac_codigoacesso`
  ADD CONSTRAINT `fk_cac_codigoacesso_pes_pesquisa1` FOREIGN KEY (`peq_id`) REFERENCES `peq_pesquisa` (`peq_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cac_codigoacesso_poa_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pes_pessoa` (`pes_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `con_convite`
--
ALTER TABLE `con_convite`
  ADD CONSTRAINT `fk_con_convite_pes_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pes_pessoa` (`pes_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_con_convite_usu_usuario1` FOREIGN KEY (`usu_id`) REFERENCES `usu_usuario` (`usu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `ctr_contrato`
--
ALTER TABLE `ctr_contrato`
  ADD CONSTRAINT `fk_ctr_contrato_emp_empresa1` FOREIGN KEY (`emp_id`) REFERENCES `mem_membro` (`mem_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctr_contrato_pla_plano1` FOREIGN KEY (`pla_id`) REFERENCES `pla_plano` (`pla_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `dis_dissertativa`
--
ALTER TABLE `dis_dissertativa`
  ADD CONSTRAINT `fk_dis_dissertativa_res_resposta1` FOREIGN KEY (`res_id`) REFERENCES `res_resposta` (`res_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `end_endereco`
--
ALTER TABLE `end_endereco`
  ADD CONSTRAINT `fk_end_endereco_usu_usuario1` FOREIGN KEY (`usu_id`) REFERENCES `usu_usuario` (`usu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `for_formulario`
--
ALTER TABLE `for_formulario`
  ADD CONSTRAINT `fk_for_formulario_tem_tema1` FOREIGN KEY (`tem_id`) REFERENCES `tem_tema` (`tem_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_for_formulario_usu_usuario1` FOREIGN KEY (`usu_id`) REFERENCES `usu_usuario` (`usu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `gpo_grupopessoa`
--
ALTER TABLE `gpo_grupopessoa`
  ADD CONSTRAINT `fk_gpo_grupopessoa_gru_grupo1` FOREIGN KEY (`gru_id`) REFERENCES `gru_grupo` (`gru_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gpo_grupopessoa_poa_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pes_pessoa` (`pes_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `gru_grupo`
--
ALTER TABLE `gru_grupo`
  ADD CONSTRAINT `fk_gru_grupo_tgr_tipogrupo1` FOREIGN KEY (`tgr_id`) REFERENCES `tgr_tipogrupo` (`tgr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gru_grupo_usu_usuario1` FOREIGN KEY (`usu_usuario_usu_id`) REFERENCES `usu_usuario` (`usu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `ima_imagem`
--
ALTER TABLE `ima_imagem`
  ADD CONSTRAINT `fk_ima_imagem_usu_usuario1` FOREIGN KEY (`usu_id`) REFERENCES `usu_usuario` (`usu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `irs_identificacaoredesocial`
--
ALTER TABLE `irs_identificacaoredesocial`
  ADD CONSTRAINT `fk_irs_identificacaoredesocial_poa_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pes_pessoa` (`pes_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_irs_identificacaoredesocial_rso_redesocial1` FOREIGN KEY (`rso_id`) REFERENCES `rso_redesocial` (`rso_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `lpa_limiteplano`
--
ALTER TABLE `lpa_limiteplano`
  ADD CONSTRAINT `fk_lpa_limiteplano_pla_plano1` FOREIGN KEY (`pla_id`) REFERENCES `pla_plano` (`pla_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lpa_limiteplano_tli_tipolimite1` FOREIGN KEY (`tli_id`) REFERENCES `tli_tipolimite` (`tli_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `lpe_limitepesquisa`
--
ALTER TABLE `lpe_limitepesquisa`
  ADD CONSTRAINT `fk_lpe_limitepesquisa_pes_pesquisa1` FOREIGN KEY (`peq_id`) REFERENCES `peq_pesquisa` (`peq_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `mem_membro`
--
ALTER TABLE `mem_membro`
  ADD CONSTRAINT `fk_mem_membro_pco_permissaocontrato1` FOREIGN KEY (`pco_id`) REFERENCES `pco_permissaocontrato` (`pco_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mem_membro_usu_usuario1` FOREIGN KEY (`usu_id`) REFERENCES `usu_usuario` (`usu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `num_numerico`
--
ALTER TABLE `num_numerico`
  ADD CONSTRAINT `fk_num_numerico_res_resposta1` FOREIGN KEY (`res_id`) REFERENCES `res_resposta` (`res_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `pag_pagamento`
--
ALTER TABLE `pag_pagamento`
  ADD CONSTRAINT `fk_pag_pagamento_ctr_contrato1` FOREIGN KEY (`ctr_id`) REFERENCES `ctr_contrato` (`ctr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `par_participantes`
--
ALTER TABLE `par_participantes`
  ADD CONSTRAINT `fk_par_participantes_pes_pesquisa1` FOREIGN KEY (`peq_id`) REFERENCES `peq_pesquisa` (`peq_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_par_participantes_poa_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pes_pessoa` (`pes_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `peq_pesquisa`
--
ALTER TABLE `peq_pesquisa`
  ADD CONSTRAINT `fk_pes_pesquisa_ape_acessopesquisa1` FOREIGN KEY (`ape_id`) REFERENCES `ape_acessopesquisa` (`ape_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pes_pesquisa_for_formulario1` FOREIGN KEY (`for_id`) REFERENCES `for_formulario` (`for_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `per_pergunta`
--
ALTER TABLE `per_pergunta`
  ADD CONSTRAINT `fk_per_pergunta_ima_imagem1` FOREIGN KEY (`ima_id`) REFERENCES `ima_imagem` (`ima_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_per_pergunta_tpe_tipopergunta1` FOREIGN KEY (`tpe_id`) REFERENCES `tpe_tipopergunta` (`tpe_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `per_pergunta_ibfk_1` FOREIGN KEY (`for_id`) REFERENCES `for_formulario` (`for_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Restrições para a tabela `pes_pessoa`
--
ALTER TABLE `pes_pessoa`
  ADD CONSTRAINT `fk_poa_pessoa_tpo_tipopessoa1` FOREIGN KEY (`tpo_id`) REFERENCES `tpo_tipopessoa` (`tpo_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `pgr_pesquisagrupo`
--
ALTER TABLE `pgr_pesquisagrupo`
  ADD CONSTRAINT `pgr_pesquisagrupo_ibfk_1` FOREIGN KEY (`gru_id`) REFERENCES `gru_grupo` (`gru_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `pgr_pesquisagrupo_ibfk_2` FOREIGN KEY (`peq_id`) REFERENCES `peq_pesquisa` (`peq_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Restrições para a tabela `pla_plano`
--
ALTER TABLE `pla_plano`
  ADD CONSTRAINT `fk_pla_plano_ima_imagem1` FOREIGN KEY (`ima_id`) REFERENCES `ima_imagem` (`ima_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `res_resposta`
--
ALTER TABLE `res_resposta`
  ADD CONSTRAINT `res_resposta_ibfk_1` FOREIGN KEY (`alt_id`) REFERENCES `alt_alternativa` (`alt_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `res_resposta_ibfk_2` FOREIGN KEY (`per_id`) REFERENCES `per_pergunta` (`per_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `res_resposta_ibfk_3` FOREIGN KEY (`peq_id`) REFERENCES `peq_pesquisa` (`peq_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `res_resposta_ibfk_4` FOREIGN KEY (`pes_id`) REFERENCES `pes_pessoa` (`pes_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Restrições para a tabela `sus_suspensao`
--
ALTER TABLE `sus_suspensao`
  ADD CONSTRAINT `fk_sus_suspensao_ctr_contrato1` FOREIGN KEY (`ctr_id`) REFERENCES `ctr_contrato` (`ctr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `tel_telefone`
--
ALTER TABLE `tel_telefone`
  ADD CONSTRAINT `fk_tel_telefone_usu_usuario1` FOREIGN KEY (`usu_id`) REFERENCES `usu_usuario` (`usu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `tem_tema`
--
ALTER TABLE `tem_tema`
  ADD CONSTRAINT `fk_tem_tema_ima_imagem1` FOREIGN KEY (`ima_id`) REFERENCES `ima_imagem` (`ima_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `usu_usuario`
--
ALTER TABLE `usu_usuario`
  ADD CONSTRAINT `fk_usu_usuario_poa_pessoa1` FOREIGN KEY (`pes_id`) REFERENCES `pes_pessoa` (`pes_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usu_usuario_tus_tipousuario1` FOREIGN KEY (`tus_id`) REFERENCES `tus_tipousuario` (`tus_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
