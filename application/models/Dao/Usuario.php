<?php

class Application_Model_Dao_Usuario {

    private $table;
    private $result;

    /**
     * @param string $apelido
     * @return Application_Model_Entity_Usuario
     */
    public function __construct() {
        $this->table = new Application_Model_Table_Usuario();
    }

    public function countUsuario() {
        return $this->result = $this->table->fetchAll()->count();
    }

    public function buscarUsuario($apelido) {
        
        if($apelido == 1){
            return $this->table->fetchAll();
        }
        //$this->dbadapter;
        $this->result = $this->table->fetchAll("usu_usuario = '" . $apelido . "'");

        if ($this->result->current() != null) {
            return false;
        } else {
            return true;
        }
    }

    public function inserir(Application_Model_Entity_Usuario $usuario) {
        try {
            $this->data = $this->convertObjArray($usuario);
            return $this->table->insert($this->data);
        } catch (Zend_Exception $e) {
            echo 'N�o foi poss�vel realizar a inser��o de usu�rio';
            $e->getMessage();
        }
    }

    public function novaSenha($senha, $id) {
        try {
            $senha = array('usu_senha' => $senha);
            $this->table = new Application_Model_Table_Usuario();
            return $this->table->update($senha, "usu_id = $id");
        } catch (Zend_Exception $e) {
            $e->getMessage();
        }
    }
    
    public function buscaTudo($id){
        try{
            $this->table = new Application_Model_Table_Usuario();
            die;
            $this->table->findReferenceRowset('Application_Model_Table_Pessoa');
            
            foreach($this->table as $row):
                echo $row->pes_email;
            endforeach;
            die;
        }catch(Zend_Exception $e){
            $e->getMessage();
        }
    }
    
    private function convertObjArray(Application_Model_Entity_Usuario $usuario) {
        return array(
            'usu_usuario' => $usuario->getUsuario(),
            'usu_senha' => $usuario->getSenha(),
            'pes_id' => $usuario->getPessoa()->getId(),
            'tus_id' => 1);
    }

}