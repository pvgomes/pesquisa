<?php

/**
 * Description of Pergunta
 *
 * @author Desenvolvimento
 */
class Application_Model_Dao_Resposta {

    private $result;
    private $table;
    
    public function __construct() {
        $this->table = new Application_Model_Table_Resposta();
    }
    
    public function inserir($resposta){
        $this->result = $this->convertObjArray($resposta);
        $this->table->insert($this->result);
    }

    /**
     * @param Application_Model_Entity_Alternativa $alternativa
     * @return array
     */
    private function convertObjArray($resposta) {
        return array(
            'res_respostadissertativa'  => $resposta->getRespostadissertativa(),
            'alt_id'                    => $resposta->getAlternativa()->getId(),
            'per_id'                    => $resposta->getPergunta()->getId(),
            'peq_id'                    => $resposta->getPesquisa()->getId(),
            'pes_id'                    => $resposta->getPessoa()->getId()
        );
    }

}

?>