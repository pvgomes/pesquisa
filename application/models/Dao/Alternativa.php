<?php

/**
 * Description of Pergunta
 *
 * @author Desenvolvimento
 */
class Application_Model_Dao_Alternativa {

    private $table;
    private $result;

    public function __construct() {
        $this->table = new Application_Model_Table_Alternativa();
    }

    public function inserir($alternativa) {
        $this->result = $this->convertObjArray($alternativa);
        return $this->table->insert($this->result);
    }

    public function deletar($id) {
        return $this->table->delete("alt_id = $id");
    }

    public function alterar($dado,$id) {
        $this->result = array( 'alt_descricao' => $dado );
        return $this->table->update($this->result, $where);
    }

    /**
     * @param Application_Model_Entity_Alternativa $alternativa
     * @return array
     */
    private function convertObjArray($alternativa) {
        return array(
            'alt_descricao' => $alternativa->getDescricao(),
            'per_id' => $alternativa->getPergunta()->getId(),
            'ima_id' => 0
        );
    }

}

?>