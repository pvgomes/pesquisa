<?php

/**
 * Description of Pesquisa
 *
 * @author Raphael
 */
class Application_Model_Dao_Pesquisa {

    private $table;
    private $result;

    public function __construct() {
        $this->table = new Application_Model_Table_Pesquisa();
    }

    public function countPesquisa() {
        return $this->result = $this->table->fetchAll("peq_datafim is null")->count();
    }

    public function nova($pesquisa) {
        $this->result = $this->convertObjtoArray($pesquisa);
        return $this->table->insert($this->result);
    }

    public function buscaPesquisaId($id, $usu_id = null) {

        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('peq' => 'peq_pesquisa'), array('*'))
                ->join(array('for' => 'for_formulario'), 'peq.for_id = for.for_id', array('*'))
                ->where("peq_id = ?", $id);

        if ($usu_id) {
            $select->where("for.usu_id = $usu_id");
        }



        foreach ($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Pesquisa();
            $this->entity->setId($row->peq_id);
            $this->entity->getFormulario()->setId($row->for_id);
            $this->entity->getFormulario()->setNome($row->for_nome);
            $this->entity->setNome($row->peq_nome);
            $this->entity->setDescricao($row->peq_descricao);
            $this->entity->setUrl($row->peq_url);

            //SÓ FAZ SET SENHA SE != NULL
            $this->entity->setSenha($row->peq_senha);
            //SÓ FAZ SET DATA SE != NULL
            $this->entity->setDatainicio($row->peq_datainicio);
            $this->entity->setDatafim($row->peq_datafim);
        endforeach;


        return $this->entity;
    }

    public function buscaPesquisaNome($nome, $usu_id) {
        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('peq' => 'peq_pesquisa'), array('*'))
                ->join(array('for' => 'for_formulario'), 'peq.for_id = for.for_id', array())
                ->where("peq_nome LIKE ?", "%$nome%")
                ->where("for.usu_id = $usu_id");

        $pesquisa = array();
        foreach ($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Pesquisa();
            $this->entity->setId($row->peq_id);
            $this->entity->setNome($row->peq_nome);

            array_push($pesquisa, $this->entity);
        endforeach;

        return $pesquisa;
    }

    public function montaurl($id) {
        $this->result = array('peq_url' => "/resposta/questionario/peq/ $id ");
        return $this->table->update($this->result, "peq_id = $id");
    }

    public function buscaPesquisaPorFormulario($id) {
        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('peq' => 'peq_pesquisa'), array('*'))
                ->where("for_id = $id");

        $formulario = array();
        foreach ($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Pesquisa();
            $this->entity->setId($row->peq_id);
            $this->entity->setNome($row->peq_nome);

            array_push($formulario, $this->entity);
        endforeach;

        return $formulario;
    }

    public function convertObjtoArray($pesquisa) {
        return array(
            'peq_nome' => $pesquisa->getNome(),
            'peq_descricao' => $pesquisa->getDescricao(),
            'peq_datainicio' => $pesquisa->getDatainicio(),
            'peq_datafim' => $pesquisa->getDatafim(),
            'for_id' => $pesquisa->getFormulario()->getId(),
            'ape_id' => $pesquisa->getAcessopesquisa()->getId()
        );
    }

    public function atualiza($pesquisa, $id) {

        //$this->result = $this->convertObjtoArray($pesquisa);
        $this->result = array(
            'peq_nome' => $pesquisa->getNome(),
            'peq_descricao' => $pesquisa->getDescricao(),
        );
        return $this->table->update($this->result, "peq_id = $id");
    }

    public function buscaPesquisaEformulario($id) {

        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('per' => 'per_pergunta'), array('*'))
                ->join(array('for' => 'for_formulario'), 'per.for_id = for.for_id', array('*'));

        if ($argumento) {
            $select->where('per.per_id = ?', $id);
        } else {
            $select->where('per.for_id = ?', $id);
        }

        $perguntas = array();
        foreach ($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Pergunta();

            $this->entity->setId($row->per_id);
            $this->entity->setDescricao($row->per_descricao);

            //SE PERGUNTA TIVER ALTERNATIVA PEGA AS ALTERNATIVAS
            if ($row->tpe_id == 1) {
                $this->table = new Application_Model_Table_Alternativa();

                $alternativas = array();
                foreach ($this->table->fetchAll('per_id = ' . $row->per_id) as $rowAlt):
                    //ADICIONO UMA ALTERNATIVA
                    //$alternativa = array($rowAlt->alt_id => $rowAlt->alt_descricao);
                    //COLOCO A ALTERNATIVA EM ALTERNATIVAS
                    $alternativas[$rowAlt->alt_id] = $rowAlt->alt_descricao;
                endforeach;

                //ADICIONA AS ALTERNATIVAS PARA A PERGUNTA
                $this->entity->addAlternativas($alternativas);
            }

            array_push($perguntas, $this->entity);

        endforeach;

        return $perguntas;
    }

    public function deletar($id) {

        $this->table->delete("peq_id = $id");
    }

    public function buscaPesquisaPorUsuario($idUsuario) {

        $select = $this->table->select();
        $select->setIntegrityCheck(false)
                ->from(array('peq' => 'peq_pesquisa'), array('*'))
                ->join(array('for' => 'for_formulario'), 'peq.for_id = for.for_id', array())
                ->where("for.usu_id = $idUsuario");

        
        $pesquisa = array();
        foreach ($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Pesquisa();
            $this->entity->setId($row->peq_id);
            $this->entity->setNome($row->peq_nome);
            
            array_push($pesquisa,  $this->entity);
        endforeach;
        
        return $pesquisa;
    }

    public function respostaPorPesquisa($idPesquisa = null){
        $select = $this->table->select();
        $select ->setIntegrityCheck(false)
                ->from(array('res' => 'res_resposta'),array('count(*) as respostas','peq_id'))
                ->join(array('peq' => 'peq_pesquisa'),'res.peq_id = peq.peq_id',array('peq_nome'))
                ->group('res.peq_id');
        
        if($idPesquisa != null and $idPesquisa != 'todos'){
            $select->where("res.peq_id = $idPesquisa");
        }
        
        return $this->table->fetchAll($select);
        
    }
    
}

?>
