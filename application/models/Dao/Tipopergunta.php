<?php

/**
 * Description of Tipo Pergunta
 *
 * @author Pv
 */
class Application_Model_Dao_Tipopergunta {

    public function __construct() {
        $this->table = new Application_Model_Table_Tipopergunta();
    }

    public function buscaTipos() {

        $result = $this->table->fetchAll();

        $todos = array();
        foreach ($result as $row):
            $tipos = new Application_Model_Entity_Tipopergunta();

            $tipos->setId($row->tpe_id);
            $tipos->setNome($row->tpe_nome);
            $tipos->setDescricao($row->tpe_descricao);


            array_push($todos, $tipos);

        endforeach;

        return $todos;
    }

}

?>
