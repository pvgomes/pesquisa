<?php

class Application_Model_Dao_Plano {

    private $table;
    private $result;

    public function __construct() {
        $this->table = new Application_Model_Table_Plano();
    }

    public function inserir($plano) {
        $this->result = $this->convertObjArray($plano);
        return $this->table->insert($this->result);
    }

    public function countPlanos() {
        return $this->result = $this->table->fetchAll("pla_status = 1")->count();
    }

    public function buscarTodos() {

        $result = $this->table->fetchAll("pla_status = 1");

        $plano = array();
        foreach ($result as $row) {
            $pla = new Application_Model_Entity_Plano();
            $pla->setNome($row->pla_nome);
            $pla->setDescricao($row->pla_descricao);
            $pla->setValor($row->pla_valor);
            $pla->getImagem()->setId($row->ima_id);
            $pla->setId($row->pla_id);

            array_push($plano, $pla);
        }

        return $plano;
    }

    public function buscarDesativados() {

        $result = $this->table->fetchAll("pla_status = 0");

        $plano = array();
        foreach ($result as $row) {
            $pla = new Application_Model_Entity_Plano();
            $pla->setNome($row->pla_nome);
            $pla->setDescricao($row->pla_descricao);
            $pla->setValor($row->pla_valor);
            $pla->getImagem()->setId($row->ima_id);
            $pla->setId($row->pla_id);

            array_push($plano, $pla);
        }

        return $plano;
    }

    public function altstatus($id, $estado) {

        $status = array('pla_status' => $estado);

        return $this->table->update($status, 'pla_id = ' . $id);
    }

    /**
     * @param Application_Model_Entity_Pessoa $pessoa
     * @return array
     */
    private function convertObjArray($plano) {
        return array(
            'pla_nome' => $plano->getNome(),
            'pla_valor' => $plano->getValor(),
            'pla_descricao' => $plano->getDescricao(),
            'pla_status' => 1,
            'ima_id' => $plano->getImagem()->getId()
        );
    }

}