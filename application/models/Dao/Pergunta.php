<?php

/**
 * Description of Pergunta
 *
 * @author Desenvolvimento
 */
class Application_Model_Dao_Pergunta {

    private $table;
    private $result;
    private $entity;

    public function __construct() {
        $this->table = new Application_Model_Table_Pergunta;
    }

    public function buscarPorFormulario($id, $argumento = null) {

        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('per' => 'per_pergunta'), array('*'))
                ->join(array('for' => 'for_formulario'), 'per.for_id = for.for_id', array('*'));

        if ($argumento) {
            $select->where('per.per_id = ?', $id);
        } else {
            $select->where('per.for_id = ?', $id);
        }

        $perguntas = array();
        foreach ($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Pergunta();

            $this->entity->setId($row->per_id);
            $this->entity->setDescricao($row->per_descricao);

            //SE PERGUNTA TIVER ALTERNATIVA PEGA AS ALTERNATIVAS
            if ($row->tpe_id == 1) {
                $this->table = new Application_Model_Table_Alternativa();

                $alternativas = array();
                foreach ($this->table->fetchAll('per_id = ' . $row->per_id) as $rowAlt):
                    //ADICIONO UMA ALTERNATIVA
                    //$alternativa = array($rowAlt->alt_id => $rowAlt->alt_descricao);
                    //COLOCO A ALTERNATIVA EM ALTERNATIVAS
                    $alternativas[$rowAlt->alt_id] = $rowAlt->alt_descricao;
                endforeach;

                //ADICIONA AS ALTERNATIVAS PARA A PERGUNTA
                $this->entity->addAlternativas($alternativas);
            }

            array_push($perguntas, $this->entity);

        endforeach;

        return $perguntas;
    }

    public function buscaQuantidadePergunta() {


        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('per' => 'per_pergunta'), array())
                ->joinRight(array('for' => 'for_formulario'), 'per.for_id = for.for_id', array('for.for_id, for.for_nome, for.for_descricao, count(for.for_id) as quantidade'))
                ->where('for.usu_id = 1')
                ->group('for.for_id');

        return $this->table->fetchAll($select);
    }

    public function novaPergunta($pergunta) {
        $this->result = $this->convertObjArray($pergunta);
        return $this->table->insert($this->result);
    }

    public function editPergunta($pergunta) {
        $where = 'per_id =' . $pergunta->getId();
        
        $this->result = array(
            'per_descricao' => $pergunta->getDescricao()
        );
        
        return $this->table->update($this->result, $where);
        
    }

    public function delPergunta($id) {
        echo $this->table->delete('per_id = ' . $id);
        exit;
    }

    public function buscaPerguntaAlternativa($idPergunta) {

        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('per' => 'per_pergunta'), array('*'))
                ->joinLeft(array('alt' => 'alt_alternativa'), 'per.per_id = alt.per_id', array('*'))
                ->where("per.per_id = $idPergunta");

        $this->table->fetchAll($select);

        foreach ($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Pergunta();
            $this->entity->setId($row->per_id);
            $this->entity->setDescricao($row->per_descricao);
        //$this->entity->setPredefinida($row->tpe_id);
        endforeach;

        return $this->entity;
    }

    /**
     * @param Application_Model_Entity_Pergunta $pergunta
     * @return array
     */
    private function convertObjArray($pergunta) {
        return array(
            'per_descricao' => $pergunta->getDescricao(),
            'tpe_id' => $pergunta->getTipopergunta()->getId(),
            'for_id' => $pergunta->getFormulario()->getId()
        );
    }

}

?>
