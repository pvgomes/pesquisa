<?php
class Application_Model_Dao_Imagem{
	
	private $table;
	private $result;

	/**
	 * @var array
	 */
	private $data;

	private $adapter;
	
	public function inserir($imagem){
		try{
			$this->data = $this->convertObjArray($imagem);		
			$this->table = new Application_Model_Table_Imagem();
			
			return $this->table->insert($this->data);
			
		}catch(Zend_Exception $e){
			echo 'Nao foi possivel';
			$e->getMessage();
		}	
	}        
	
	/**
	 * @param Application_Model_Entity_Pessoa $pessoa
	 * @return array
	 */
	private function convertObjArray($imagem){
		return array(	
                                'ima_nome'      => $imagem->getNome(),
                                'ima_descricao' => $imagem->getDescricao(),
                                'ima_criacao'	=> $imagem->getCriacao(),
                                'usu_id'        => $imagem->getUsuario()->getId());
	}

}