<?php

/**
 * Description of Tema
 *
 * @author Raphael
 */
class Application_Model_Dao_Tema {

    private $table;
    private $result;

    public function __construct() {
        $this->table = new Application_Model_Table_Tema();
    }

    public function buscaTema() {

        $sql = $this->table->select();
        $sql->setIntegrityCheck(false);
        $sql->from(array('tem' => 'tem_tema'), array('*'))
            ->join(array('ima' => 'ima_imagem'),'tem.ima_id = ima.ima_id',array('*'));        
        
        $result = $this->table->fetchAll($sql);

        $tema = array();

        foreach ($result as $row):
         
            $tem = new Application_Model_Entity_Tema();
            $tem->setId($row->tem_id);
            $tem->setNomefantasia($row->tem_nomefantasia);
            $tem->getImagem()->setId($row->ima_id);
            $tem->getImagem()->setNome($row->ima_nome);

            array_push($tema, $tem);

        endforeach;


        return $tema;
    }

}

?>
