<?php

class Application_Model_Dao_Formulario {

    private $result;
    private $table;

    public function __construct() {
        $this->table = new Application_Model_Table_Formulario();
    }

    public function countModelo() {
        return $this->result = $this->table->fetchAll()->count();
    }

    public function deletar($id) {
        // ESSE DELETE DEVE SER TODO CASCADE
        $where = 'for_id = ' . $id;
        $pergunta = new Application_Model_Table_Pergunta();
        $pergunta->delete($where);

        return $this->result = $this->table->delete($where);
    }

    public function inserir(Application_Model_Entity_Formulario $formulario) {
        try {
            $this->data = $this->convertObjArray($formulario);
            return $this->table->insert($this->data);
            exit;
        } catch (Zend_Exception $e) {
            echo '<h1 style="color:#f00">Não foi possível criar um formulario</h1>';
            $e->getMessage();
        }
    }

    public function alterar($id, $nome, $descricao) {

        $dados = array(
                        "for_nome" => $nome,
                        "for_descricao" => $descricao
                        );
        
        $where = "for_id = $id";

        $this->table->update($dados, $where);
        return $id;
    }

    public function buscarUmFormulario($id) {
        $where = "for_id = " . $id;
        $result = $this->table->fetchAll($where);

        foreach ($result as $row):
            $form = new Application_Model_Entity_Formulario();
            $form->setDescricao($row->for_descricao);
            $form->setNome($row->for_nome);
        endforeach;

        //retorna o objeto formulario
        return $form;
    }

    public function buscarFormularios($id = null) {

        if ($id) {
            $where = "usu_id = $id";
            $result = $this->table->fetchAll($where);
        } else {
            $result = $this->table->fetchAll();
        }

        $formulario = array();
        foreach ($result as $row):
            $form = new Application_Model_Entity_Formulario();
            $form->setDescricao($row->for_descricao);
            $form->setNome($row->for_nome);
            $form->setId($row->for_id);

            array_push($formulario, $form);

        endforeach;

        return $formulario;
    }

    public function buscaFormularioModelo() {

        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('for' => 'for_formulario'), array('*'))
                ->join(array('usu' => 'usu_usuario'), 'for.usu_id = usu.usu_id', array())
                ->join(array('tus' => 'tus_tipousuario'), 'usu.tus_id = tus.tus_id', array())
                ->where('usu.tus_id = 3');

        $formulario = array();
        foreach ($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Formulario();
            $this->entity->setId($row->for_id);
            $this->entity->setNome($row->for_nome);

            array_push($formulario, $this->entity);
        endforeach;

        return $formulario;
    }

    public function modelo($id) {

        //FAZ O SELECT COM O ID DO FORM MODELO
        //PARA PEGAR TUDO QUE TEM NESSE FORM E FAZER UMA COPIA
        $select = $this->table->select();
        $select->setIntegrityCheck(false);
        $select->from(array('for' => 'for_formulario'), array('*'))
                ->where("for.for_id = $id");

        foreach ($this->table->fetchAll($select) as $row):
            $insert = array(
                "for_nome" => $row->for_nome,
                "for_descricao" => $row->for_descricao,
                "usu_id" => $row->usu_id,
                "tem_id" => $row->tem_id,
            );

            $idformulario = $this->table->insert($insert);

            //PEGA AS PERGUNTAS QUE TEM NO FORM MODELO
            $this->table = new Application_Model_Table_Pergunta();
            $select = $this->table->select();
            $select->setIntegrityCheck(false);
            $select->from(array('per' => 'per_pergunta'), array('*'))
                    ->where("per.for_id = $row->for_id");

            foreach ($this->table->fetchAll($select) as $rowPer):
                //SE ELE TIVER PERGUNTA, PEGA AS PERGUNTAS E COLOCA NO NOVO FORM
                if ($rowPer->per_id) {

                    $insertPergunta = array(
                        "per_descricao" => $rowPer->per_descricao,
                        "tpe_id" => $rowPer->tpe_id,
                        "for_id" => $idformulario,
                        "ima_id" => $rowPer->ima_id,
                    );

                    $this->table = new Application_Model_Table_Pergunta();
                    $idPergunta = $this->table->insert($insertPergunta);

                    //PEGA AS ALTERNATIVAS DA PERGUNTA DO FORM MODELO
                    $this->table = new Application_Model_Table_Alternativa();
                    $select = $this->table->select();
                    $select->setIntegrityCheck(false);
                    $select->from(array('alt' => 'alt_alternativa'), array('*'))
                            ->where("alt.per_id = $rowPer->per_id");


                    foreach ($this->table->fetchAll($select) as $rowAlt):
                        //SE A PERGUNTA TIVER ALTERNATIVA
                        if ($rowAlt->alt_id) {

                            $insertAlternativa = array(
                                "alt_descricao" => $rowAlt->alt_descricao,
                                "per_id" => $idPergunta,
                                "ima_id" => $rowAlt->ima_id
                            );

                            $this->table = new Application_Model_Table_Alternativa();
                            $this->table->insert($insertAlternativa);
                        }
                    endforeach;
                }
            endforeach;
        endforeach;

    }
    
    public function pegaNome($id){
        $select = $this->table->select();
        $select ->setIntegrityCheck(false);
        $select ->from(array('for'=>'for_formulario',array('*')))
                ->where("for_id = $id");
                
        $formulario = array();
        foreach($this->table->fetchAll($select) as $row):
            $this->entity = new Application_Model_Entity_Formulario();
            $this->entity->setId($row->for_id);
            $this->entity->setNome($row->for_nome);
            $this->entity->setDescricao($row->for_descricao);
            
            array_push($formulario, $this->entity);
        endforeach;
        return $formulario;
        
    }
    
    private function convertObjArray(Application_Model_Entity_Formulario $formulario) {
        $tema = 1;
        if ($formulario->getTema()->getId() != null) {
            $tema = $formulario->getTema()->getId();
        }

        return array(
            'for_nome' => $formulario->getNome(),
            'for_descricao' => $formulario->getDescricao(),
            'usu_id' => $formulario->getUsuario()->getId(),
            'tem_id' => $tema
        );
    }

}

?>
