<?php

class Application_Model_Dao_Pessoa {

    private $table;
    private $result;

    /**
     * @var array
     */
    private $data;
    private $adapter;

    public function inserir($pessoa) {
        try {
            $this->data = $this->convertObjArray($pessoa);
            $this->table = new Application_Model_Table_Pessoa();

            return $this->table->insert($this->data);
        } catch (Zend_Exception $e) {
            echo 'Nao foi possivel';
            $e->getMessage();
        }
    }

    public function buscaEmail($email) {

        $this->table = new Application_Model_Table_Pessoa();
        $this->result = $this->table->fetchAll("pes_email = '" . $email . "'");

        if ($this->result->current() != null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param Application_Model_Entity_Pessoa $pessoa
     * @return array
     */
    private function convertObjArray($pessoa) {
        return array(
            'pes_email' => $pessoa->getEmail(),
            'pes_nome' => $pessoa->getNome(),
            'pes_datanascimento' => $pessoa->getDatanascimento(),
            'tpo_id' => 1);
    }

}