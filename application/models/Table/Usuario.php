<?php

require_once 'Zend/Db/Table/Abstract.php';

/**
 * Description of Usuario
 *
 * @author Raphael
 */
class Application_Model_Table_Usuario extends Zend_Db_Table_Abstract {

    protected $_name = 'usu_usuario';
    protected $_primary = 'usu_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Pessoa' =>
        array('columns' => array('pes_id'),
            'refTableClass' => 'Application_Model_Table_Pessoa',
            'refColumns' => array('pes_id')
        ),
        'Application_Model_Table_Tipousuario' =>
        array('columns' => array('tus_id'),
            'refTableClass' => 'Application_Model_Table_Tipousuario',
            'refColumns' => array('tus_id')
        )
    );
    protected $_dependentTables = array(
        'Application_Model_Table_Contrato',
        'Application_Model_Table_Convite',
        'Application_Model_Table_Formulario',
        'Application_Model_Table_Imagem',
        'Application_Model_Table_Endreco',
        'Application_Model_Table_Telefone',
        );

}

?>
