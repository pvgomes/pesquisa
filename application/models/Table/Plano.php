<?php

/**
 * Description of Plano
 *
 * @author Raphael
 */
class Application_Model_Table_Plano extends Zend_Db_Table_Abstract {

    protected $_name = 'pla_plano';
    protected $_primary = 'pla_id';
    
    protected $_referenceMap = array(
        'Application_Model_Table_Imagem' =>
        array('columns' => array('ima_id'),
            'refTableClass' => 'Application_Model_Table_Imagem',
            'refColumns' => array('ima_id')
        )
    );
    protected $_dependentTables = array(
        'Application_Model_Table_Contrato',
        'Application_Model_Table_Limiteplano');

}

?>
