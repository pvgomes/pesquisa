<?php

class Application_Model_Table_Formulario extends Zend_Db_Table_Abstract {

    protected $_name = 'for_formulario';
    protected $_primary = 'for_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Usuario' =>
        array('columns' => array('usu_id'),
            'refTableClass' => 'Application_Model_Table_Usuario',
            'refColumns' => array('usu_id')
        ),
        'Application_Model_Table_Tema' =>
        array(
            'columns' => array('tem_id'),
            'refTableClass' => 'Application_Model_Table_Tema',
            'refColumns' => array('tem_id')
        )
    );
    protected $_dependentTables = array('Application_Model_Table_Pergunta',
        'Application_Model_Table_Pesquisa'
    );

}