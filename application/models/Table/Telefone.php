<?php
/**
 * Description of Telefone
 *
 * @author Raphael
 */
class Application_Model_Table_Telefone extends Zend_Db_Table_Abstract{
    protected $_name    = 'tel_telefone';
    protected $_primary = 'tel_id';
    
    //chave estrangeira que tem na tabela
    protected $_referenceMap = array(
        'Application_Model_Table_Usuario' =>
        array('columns' => array('usu_id'),
            'refTableClass' => 'Application_Model_Table_Usuario',
            'refColumns' => array('usu_id')
        )
    );
}

?>
