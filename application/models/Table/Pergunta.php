<?php

/**
 * Description of Pergunta
 *
 * @author Raphael
 */
class Application_Model_Table_Pergunta extends Zend_Db_Table_Abstract {

    protected $_name = 'per_pergunta';
    protected $_primary = 'per_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Tipopergunta' =>
        array('columns' => array('tpe_id'),
            'refTableClass' => 'Application_Model_Table_Tipopergunta',
            'refColumns' => array('tpe_id')
        ),
        'Application_Model_Table_Formulario' =>
        array(
            'columns' => array('for_id'),
            'refTableClass' => 'Application_Model_Table_Formulario',
            'refColumns' => array('for_id')
        ),
        'Application_Model_Table_Imagem' =>
        array(
            'columns' => array('ima_id'),
            'refTableClass' => 'Application_Model_Table_Imagem',
            'refColumns' => array('ima_id')
        )
    );
    protected $_dependentTables = array(
        'Application_Model_Table_Alternativa',
        'Application_Model_Table_Resposta'
    );

}

?>
