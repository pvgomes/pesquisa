<?php
/**
 * Description of Tipolimite
 *
 * @author Raphael
 */
class Application_Model_Table_Tipolimite extends Zend_Db_Table_Abstract {
    protected $_name    = 'tli_tipolimite';
    protected $_primary = 'tli_id';
    
    protected $_dependentTables = array('Application_Model_Table_Tipolimite');
}

?>
