<?php
/**
 * Description of Tipopergunta
 *
 * @author Raphael
 */
class Application_Model_Table_Tipopergunta extends Zend_Db_Table_Abstract {
    protected $_name    = 'tpe_tipopergunta';
    protected $_primary = 'tpe_id';
    
    protected $_dependentTables = array('Application_Model_Table_Pergunta');
}

?>
