<?php

class Application_Model_Table_Grupo extends Zend_Db_Table_Abstract {

    protected $_name = 'gru_grupo';
    protected $_primary = 'gru_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Tipogrupo' =>
        array('columns' => array('tgr_id'),
            'refTableClass' => 'Application_Model_Table_Tipogrupo',
            'refColumns' => array('tgr_id')
        )
    );
    protected $_dependentTables = array('Application_Model_Table_Grupopessoa',
        'Application_Model_Table_Pesquisagrupo'
    );

}