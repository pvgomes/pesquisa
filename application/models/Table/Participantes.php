<?php
/**
 * Description of Participantes
 *
 * @author Raphael
 */
class Application_Model_Table_Participantes extends Zend_Db_Table_Abstract {
    
    protected $_name    ='par_participantes';
    protected $_primary ='par_id';
    
    protected $_referenceMap = array(
        'Application_Model_Table_Pesquisa' =>
        array('columns' => array('peq_id'),
            'refTableClass' => 'Application_Model_Table_Pesquisa',
            'refColumns' => array('peq_id')
        ),
        'Application_Model_Table_Pessoa' =>
        array(
            'columns' => array('pes_id'),
            'refTableClass' => 'Application_Model_Table_Pessoa',
            'refColumns' => array('pes_id')
        )
    );
}

?>
