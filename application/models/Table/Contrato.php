<?php

class Application_Model_Table_Contrato extends Zend_Db_Table_Abstract {

    protected $_name = 'ctr_contrato';
    protected $_primary = 'ctr_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Usuario' =>
        array(
            'columns' => array('usu_id'),
            'refTableClass' => 'Application_Model_Table_Usuario',
            'refColumns' => array('usu_id')
        ),
        'Application_Model_Table_Plano' =>
        array(
            'columns' => array('pla_id'),
            'refTableClass' => 'Application_Model_Table_Plano',
            'refColumns' => array('pla_id')
        )
    );
    protected $_dependentTables = array('Application_Model_Table_Pagamento',
        'Application_Model_Table_Suspensao'
    );

}