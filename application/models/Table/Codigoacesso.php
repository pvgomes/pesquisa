<?php

class Application_Model_Table_Codigoacesso extends Zend_Db_Table_Abstract {

    protected $_name = 'cac_codigoacesso';
    protected $_primary = 'cac_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Pessoa' =>
        array('columns' => array('pes_id'),
            'refTableClass' => 'Application_Model_Table_Pessoa',
            'refColumns' => array('pes_id')
        ),
        'Application_Model_Table_Pesquisa' =>
        array(
            'columns' => array('peq_id'),
            'refTableClass' => 'Application_Model_Table_Pesquisa',
            'refColumns' => array('peq_id')
        )
    );

}