<?php
/**
 * Description of Tipogrupo
 *
 * @author Raphael
 */
class Application_Model_Table_Tipogrupo extends Zend_Db_Table_Abstract {
    protected $_name    = 'tgr_tipogrupo';
    protected $_primary = 'tgr_id';
    
    protected $_dependentTables = array('Application_Model_Table_Grupo');

}

?>
