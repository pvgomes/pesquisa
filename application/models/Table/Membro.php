<?php

/**
 * Description of Membro
 *
 * @author Raphael
 */
class Application_Model_Table_Membro extends Zend_Db_Table_Abstract {

    protected $_name = 'mem_membro';
    protected $_primary = 'mem_id';
    //chave estrangeira que tem na tabela
    protected $_referenceMap = array(
        'Application_Model_Table_Usuario' =>
        array('columns' => array('usu_id'),
            'refTableClass' => 'Application_Model_Table_Usuario',
            'refColumns' => array('usu_id')
        ),
        'Application_Model_Table_Permissaocontrato' =>
        array(
            'columns' => array('pco_id'),
            'refTableClass' => 'Application_Model_Table_Permissaocontrato',
            'refColumns' => array('pco_id')
        )
    );

}

?>
