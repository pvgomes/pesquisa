<?php
/**
 * Description of Redesocial
 *
 * @author Raphael
 */
class Application_Model_Table_Redesocial extends Zend_Db_Table_Abstract {
    protected $_name    = 'rso_redesocial';
    protected $_primary = 'rso_id';
   
    protected $_dependentTables = array('Application_Model_Table_Identificacaoredesocial');
}

?>
