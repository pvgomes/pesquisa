<?php

class Application_Model_Table_Limitepesquisa extends Zend_Db_Table_Abstract {

    protected $_name = 'lpe_limitepesquisa';
    protected $_primary = 'lpe_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Pesquisa' =>
        array(
            'columns' => array('peq_id'),
            'refTableClass' => 'Application_Model_Table_Pesquisa',
            'refColumns' => array('peq_id')
        )
    );

}