<?php

class Application_Model_Table_Pagamento extends Zend_Db_Table_Abstract {

    protected $_name = 'pag_pagamento';
    protected $_primary = 'pag_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Contrato' =>
        array('columns' => array('ctr_id'),
            'refTableClass' => 'Application_Model_Table_Contrato',
            'refColumns' => array('ctr_id')
        )
    );

}