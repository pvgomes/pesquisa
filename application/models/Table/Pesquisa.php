<?php

/**
 * Description of Pesquisa
 *
 * @author Raphael
 */
class Application_Model_Table_Pesquisa extends Zend_Db_Table_Abstract {

    protected $_name = 'peq_pesquisa';
    protected $_primary = 'peq_id';
    //chave estrangeira que tem na tabela
    protected $_referenceMap = array(
        'Application_Model_Table_Formulario' =>
        array('columns' => array('for_id'),
            'refTableClass' => 'Application_Model_Table_Formulario',
            'refColumns' => array('for_id')
        ),
        'Application_Model_Table_Acessopesquisa' =>
        array(
            'columns' => array('ape_id'),
            'refTableClass' => 'Application_Model_Table_Acessopesquisa',
            'refColumns' => array('ape_id')
        )
    );
    //chave primaria dessa tabela que vai para as tabelas abaixo
    protected $_dependentTables = array(
        'Application_Model_Table_Codigoacesso',
        'Application_Model_Table_Limitepesquisa',
        'Application_Model_Table_Participantes',
        'Application_Model_Table_Pesquisagrupo',
        'Application_Model_Table_Resposta'
    );

}

?>
