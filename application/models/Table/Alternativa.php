<?php

class Application_Model_Table_Alternativa extends Zend_Db_Table_Abstract {

    protected $_name = 'alt_alternativa';
    protected $_primary = 'alt_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Pergunta' =>
        array('columns' => array('per_id'),
            'refTableClass' => 'Application_Model_Table_Pergunta',
            'refColumns' => array('per_id')
        ),
        'Application_Model_Table_Imagem' =>
        array(
            'columns' => array('ima_id'),
            'refTableClass' => 'Application_Model_Table_Imagem',
            'refColumns' => array('ima_id')
        )
    );
    protected $_dependentTables = array('Application_Model_Table_Resposta');

}