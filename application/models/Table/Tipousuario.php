<?php
/**
 * Description of Tipousuario
 *
 * @author Raphael
 */
class Application_Model_Table_Tipousuario extends Zend_Db_Table_Abstract{
    protected $_name    = 'tus_tipousuario';
    protected $_primary = 'tus_id';
    
    protected $_dependentTables = array('Application_Model_Table_Usuario');
}

?>
