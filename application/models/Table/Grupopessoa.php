<?php

class Application_Model_Table_Grupopessoa extends Zend_Db_Table_Abstract {

    protected $_name = 'gpo_grupopessoa';
    protected $_primary = 'gpo_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Grupo' =>
        array('columns' => array('gru_id'),
            'refTableClass' => 'Application_Model_Table_Grupo',
            'refColumns' => array('gru_id')
        )
    );

}