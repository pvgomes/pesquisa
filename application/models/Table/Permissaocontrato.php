<?php

/**
 * Description of Permissaocontrato
 *
 * @author Raphael
 */
class Application_Model_Table_Permissaocontrato extends Zend_Db_Table_Abstract {

    protected $_name = 'pco_permissaocontrato';
    protected $_primary = 'pco_id';
    protected $_dependentTables = array('Application_Model_Table_Membro');

}

?>
