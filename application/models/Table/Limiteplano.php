<?php

class Application_Model_Table_Limiteplano extends Zend_Db_Table_Abstract {

    protected $_name = 'lpa_limiteplano';
    protected $_primary = 'lpa_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Tipolimite' =>
        array('columns' => array('tli_id'),
            'refTableClass' => 'Application_Model_Table_Tipolimite',
            'refColumns' => array('tli_id')
        ),
        'Application_Model_Table_Plano' =>
        array(
            'columns' => array('pla_id'),
            'refTableClass' => 'Application_Model_Table_Plano',
            'refColumns' => array('pla_id')
        )
    );

}