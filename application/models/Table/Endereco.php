<?php

/**
 * Description of Endereco
 *
 * @author Raphael
 */
class Application_Model_Table_Endereco extends Zend_Db_Table_Abstract {

    protected $_name = 'end_endereco';
    protected $_primary = 'end_id';
    //chave estrangeira que tem na tabela
    protected $_referenceMap = array(
        'Application_Model_Table_Usuario' =>
        array('columns' => array('usu_id'),
            'refTableClass' => 'Application_Model_Table_Usuario',
            'refColumns' => array('usu_id')
        )
    );

}

?>
