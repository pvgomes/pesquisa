<?php

require_once 'Zend/Db/Table/Abstract.php';

/**
 * Description of Pessoa
 *
 * @author Raphael
 */
class Application_Model_Table_Pessoa extends Zend_Db_Table_Abstract {

    protected $_name = 'pes_pessoa';
    protected $_primary = 'pes_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Tipopessoa' =>
        array('columns' => array('tpo_id'),
            'refTableClass' => 'Application_Model_Table_Tipopessoa',
            'refColumns' => array('tpo_id')
        )
    );
    protected $_dependentTables = array(
        'Application_Model_Table_Codigoacesso',
        'Application_Model_Table_Convite',
        'Application_Model_Table_Identificacaoredesocial',
        'Application_Model_Table_Participantes',
        'Application_Model_Table_Resposta',
        'Application_Model_Table_Usuario'
    );

}

?>
