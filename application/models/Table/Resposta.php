<?php

/**
 * Description of Resposta
 *
 * @author Raphael
 */
class Application_Model_Table_Resposta extends Zend_Db_Table_Abstract {

    protected $_name = 'res_resposta';
    protected $_primary = 'res_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Alternativa' =>
        array('columns' => array('alt_id'),
            'refTableClass' => 'Application_Model_Table_Alternativa',
            'refColumns' => array('alt_id')
        ),
        'Application_Model_Table_Pergunta' =>
        array(
            'columns' => array('per_id'),
            'refTableClass' => 'Application_Model_Table_Pergunta',
            'refColumns' => array('per_id')
        ),
        'Application_Model_Table_Pesquisa' =>
        array(
            'columns' => array('peq_id'),
            'refTableClass' => 'Application_Model_Table_Pesquisa',
            'refColumns' => array('peq_id')
        ),
        'Application_Model_Table_Pessoa' =>
        array(
            'columns' => array('pes_id'),
            'refTableClass' => 'Application_Model_Table_Pessoa',
            'refColumns' => array('pes_id')
        )
    );
    protected $_dependentTables = array(
        'Application_Model_Table_Dissertativa',
        'Application_Model_Table_Numerico');

}

?>
