<?php

class Application_Model_Table_Numerico extends Zend_Db_Table_Abstract {

    protected $_name = 'num_numerico';
    protected $_primary = 'num_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Resposta' =>
        array('columns' => array('res_id'),
            'refTableClass' => 'Application_Model_Table_Resposta',
            'refColumns' => array('res_id')
        )
    );

}