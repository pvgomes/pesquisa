<?php

class Application_Model_Table_Convite extends Zend_Db_Table_Abstract {

    protected $_name = 'con_convite';
    protected $_primary = 'con_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Usuario' =>
        array('columns' => array('usu_id'),
            'refTableClass' => 'Application_Model_Table_Usuario',
            'refColumns' => array('usu_id')
        ),
        'Application_Model_Table_Pessoa' =>
        array(
            'columns' => array('pes_id'),
            'refTableClass' => 'Application_Model_Table_Pessoa',
            'refColumns' => array('pes_id')
        )
    );

}