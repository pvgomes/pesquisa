<?php
/**
 * Description of Suspensao
 *
 * @author Raphael
 */
class Application_Model_Table_Suspensao extends Zend_Db_Table_Abstract {
    protected $_name    = 'sus_suspensao';
    protected $_primary = 'sus_id';
    
    protected $_referenceMap = array(
        'Application_Model_Table_Contrato' =>
        array('columns' => array('ctr_id'),
            'refTableClass' => 'Application_Model_Table_Contrato',
            'refColumns' => array('ctr_id')
        )
    );
}

?>
