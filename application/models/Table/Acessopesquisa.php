<?php
/*
 * 
 * @autor Pedro Ribeiro 
 */
class Application_Model_Table_Acessopesquisa extends Zend_Db_Table_Abstract{
	
	protected $_name = 'ape_acessopesquisa';
	protected $_primary = 'ape_id';
        
        protected $_dependentTables = array('Application_Model_Table_Pesquisa');	

}