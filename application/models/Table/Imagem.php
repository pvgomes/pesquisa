<?php

class Application_Model_Table_Imagem extends Zend_Db_Table_Abstract {

    protected $_name = 'ima_imagem';
    protected $_primary = 'ima_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Usuario' =>
        array('columns' => array('usu_id'),
            'refTableClass' => 'Application_Model_Table_Usuario',
            'refColumns' => array('usu_id')
        )
    );
    protected $_dependentTables = array('Application_Model_Table_Alternativa',
        'Application_Model_Table_Pergunta',
        'Application_Model_Table_Plano',
        'Application_Model_Table_Tema'
    );

}