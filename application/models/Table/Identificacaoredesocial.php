<?php

class Application_Model_Table_Identificacaoredesocial extends Zend_Db_Table_Abstract {

    protected $_name = 'irs_identificacaoredesocial';
    protected $_primary = 'irs_id';
    protected $_referenceMap = array(
        'Application_Model_Table_Redesocial' =>
        array('columns' => array('rso_id'),
            'refTableClass' => 'Application_Model_Table_Redesocial',
            'refColumns' => array('rso_id')
        ),
        'Application_Model_Table_Pessoa' =>
        array('columns' => array('pes_id'),
            'refTableClass' => 'Application_Model_Table_Pessoa',
            'refColumns' => array('pes_id')
        )
    );

}