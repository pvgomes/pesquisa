<?php
/**
 * Description of Tema
 *
 * @author Raphael
 */
class Application_Model_Table_Tema extends Zend_Db_Table_Abstract {
    protected $_name    = 'tem_tema';
    protected $_primary = 'tem_id';
    
    protected $_referenceMap = array(
        'Application_Model_Table_Imagem' =>
        array('columns' => array('ima_id'),
            'refTableClass' => 'Application_Model_Table_Imagem',
            'refColumns' => array('ima_id')
        )
    );
    
    protected $_dependentTables = array('Application_Model_Table_Formulario');
}

?>
