<?php
/**
 * Description of Tipopessoa
 *
 * @author Raphael
 */
class Application_Model_Table_Tipopessoa extends Zend_Db_Table_Abstract {
    protected $_name    = 'tpo_tipopessoa';
    protected $_primary = 'tpo_id';
    
    protected $_dependentTables = array('Application_Model_Table_Pessoa');
    
}

?>
