<?php
/**
 * Description of Pesquisagrupo
 *
 * @author Raphael
 */
class Application_Model_Table_Pesquisagrupo extends Zend_Db_Table_Abstract {
    protected $_name    = 'pqr_pesquisagrupo';
    protected $_primary = 'pqr_id';
    
    protected $_referenceMap = array(
        'Application_Model_Table_Grupo' =>
        array('columns' => array('gru_id'),
            'refTableClass' => 'Application_Model_Table_Grupo',
            'refColumns' => array('gru_id')
        ),
        'Application_Model_Table_Pesquisa' =>
        array(
            'columns' => array('peq_id'),
            'refTableClass' => 'Application_Model_Table_Pesquisa',
            'refColumns' => array('peq_id')
        )
    );
}

?>
