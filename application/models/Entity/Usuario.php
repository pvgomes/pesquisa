<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Raphael
 */
class Application_Model_Entity_Usuario {
    private $id;
    private $usuario;
    private $senha;
    
    /**
     *
     * @var Application_Model_Entity_Pessoa
     */
    private $pessoa;
    
    /**
     *
     * @var Application_Model_Entity_Tipousuario
     */
    private $tipousario;
    
	public function __construct(){
		$this->pessoa = new Application_Model_Entity_Pessoa();
	}
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }

    
    
    public function getPessoa() {
        return $this->pessoa;
    }
    /**
     *
     * @param Application_Model_Entity_Pessoa $pessoa 
     */
    public function setPessoa(Application_Model_Entity_Pessoa $pessoa) {
        $this->pessoa = $pessoa;
    }

    
    
    public function getTipousario() {
        return $this->tipousario;
    }
    /**
     *
     * @param Application_Model_Entity_Tipousuario $tipousario 
     */
    public function setTipousario(Application_Model_Entity_Tipousuario $tipousario) {
        $this->tipousario = $tipousario;
    }


}

?>
