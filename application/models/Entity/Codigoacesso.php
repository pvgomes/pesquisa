<?php
/*
 *  @autor Paulo Victor
 */

class Application_Model_Entity_Codigoacesso{

	private $id;
	private $codigo;
	
	/*
	 * 
	 * @var Application_Model_Entity_Pessoa
	 */
	private $pessoa;
	
	/*
	 * 
	 * @var Application_Model_Entity_Pesquisa
	 */
	private $pesquisa;
	
	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}
	
	public function getCodigo(){
		return $this->codigo;
	}
	
	public function setCodigo($codigo){
		$this->codigo = $codigo;
	}
	
	/*
	 * 
	 * @return Application_Model_Entity_Pessoa
	 */
	public function getPessoa(){
		return $this->pessoa;
	}
	
	/*
	 * 
	 * @param Application_Model_Entity_Pessoa $pessoa
	 */
	public function setPessoa(Application_Model_Entity_Pessoa $pessoa){
		$this->pessoa = $pessoa;
	}
	
	
	/*
	 * 
	 * @return Application_Model_Entity_Pesquisa
	 */
	public function getPesquisa(){
		return $this->pesquisa;
	}
	
	/*
	 * 
	 *  @param Application_Model_Entity_Pesquisa $pesquisa
	 */
	public function setPesquisa(Application_Model_Entity_Pesquisa $pesquisa){
		$this->pesquisa = $pesquisa;
	}
	
	
}

?>