<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pessoa
 *
 * @author Raphael
 */
class Application_Model_Entity_Pessoa {
    private $id;
    private $nome;
    private $email;
    private $datanascimento;
    /**
     *
     * @var Application_Model_Entity_Tipopessoa 
     */
    private $tipopessoa;
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getDatanascimento() {
        return $this->datanascimento;
    }

    public function setDatanascimento($datanascimento) {
        $this->datanascimento = $datanascimento;
    }

    
    public function getTipopessoa() {
        return $this->tipopessoa;
    }
    /**
     *
     * @param Application_Model_Entity_Tipopessoa $tipopessoa 
     */
    public function setTipopessoa(Application_Model_Entity_Tipopessoa $tipopessoa) {
        $this->tipopessoa = $tipopessoa;
    }


}

?>
