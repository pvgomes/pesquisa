<?php
/**
 * Description of Membro
 *
 * @author Raphael
 */
class Application_Model_Entity_Membro {
    
    private $id;
    private $iniciovigencia;
    private $fimvigencia;
    /**
     *
     * @var Application_Model_Entity_Usuario 
     */
    private $usuario;
    /**
     *
     * @var Application_Model_Entity_Permissaocontrato
     */
    private $permissaocontrato;
    
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getIniciovigencia() {
        return $this->iniciovigencia;
    }

    public function setIniciovigencia($iniciovigencia) {
        $this->iniciovigencia = $iniciovigencia;
    }

    public function getFimvigencia() {
        return $this->fimvigencia;
    }

    public function setFimvigencia($fimvigencia) {
        $this->fimvigencia = $fimvigencia;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario(Application_Model_Entity_Usuario $usuario) {
        $this->usuario = $usuario;
    }

    public function getPermissaocontrato() {
        return $this->permissaocontrato;
    }

    public function setPermissaocontrato(Application_Model_Entity_Permissaocontrato $permissaocontrato) {
        $this->permissaocontrato = $permissaocontrato;
    }


}

?>
