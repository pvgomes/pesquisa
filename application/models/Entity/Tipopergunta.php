<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tipopergunta
 *
 * @author Raphael
 */
class Application_Model_Entity_Tipopergunta {
    private $id;
    private $descricao;
    private $nome;
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function getNome(){
        return $this->nome;
        
    }
    
    public function setNome($nome){
        $this->nome = $nome;
    }



    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }


}

?>
