<?php
class Application_Model_Entity_Grupo{

	private $id;
	private $nome;
	
	/*
	 * @var Application_Model_Entity_Usuario
	 */
	private $usuario;
	
	/*
	 * @var Application_Model_Entity_Tipogrupo
	 */
	private $tipogrupo;
	
	public function getId() { 
		return $this->id; 
	}

	public function setId($id) { 
		$this->id = $id; 
	} 
	
	
	public function getNome() { 
		return $this->nome; 
	} 
	
	public function setNome($nome) { 
		$this->nome = $nome; 
	} 
	
	
	/*
	 * @return Application_Model_Entity_Usuario
	 */
	public function getUsuario() { 
		return $this->usuario; 
	} 
	
	/*
	 * @return Application_Model_Entity_Usuario $usuario
	 */
	public function setUsuario(Application_Model_Entity_Usuario $usuario) { 
		$this->usuario = $usuario; 
	} 
	
	
	/*
	 * @return Application_Model_Entity_Tipogrupo
	 */
	public function getTipogrupo() { 
		return $this->tipogrupo; 
	} 
	
	/*
	 * 
	 * @param Application_Model_Entity_Tipogrupo $tipogrupo
	 */
	public function setTipogrupo(Application_Model_Entity_Tipogrupo $tipogrupo) { 
		$this->tipogrupo = $tipogrupo; 
	} 
	
	
	
	
	
}
?>