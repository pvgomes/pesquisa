<?php
class Application_Model_Entity_Limitepesquisa{

	private $id;
	private $limite;
	
	/*
	 * @var Application_Model_Entity_Pesquisa
	 */
	private $pesquisa;
	
	
	public function getId() { 
		return $this->id; 
	} 
	public function setId($id) { 
		$this->id = $id; 
	}
	
	
	public function getLimite() { 
		return $this->limite; 
	} 
	public function setLimite($limite) { 
		$this->limite = $limite; 
	} 
	
	/*
	 * @return Application_Model_Entity_Pesquisa
	 */
	public function getPesquisa() { 
		return $this->pesquisa; 
	} 
	
	/*
	 * @param Application_Model_Entity_Pesquisa $pesquisa
	 */
	public function setPesquisa(Application_Model_Entity_Pesquisa $pesquisa) { 
		$this->pesquisa = $pesquisa; 
	} 
	
	
}
?>