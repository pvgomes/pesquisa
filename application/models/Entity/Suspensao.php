<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Suspensao
 *
 * @author Raphael
 */
class Application_Model_Entity_Suspensao {
    private $id;
    private $datasuspensao;
    private $datareativacao;
    
    /**
     *
     * @var Application_Model_Entity_Contrato
     */
    private $contrato;
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getDatasuspensao() {
        return $this->datasuspensao;
    }

    public function setDatasuspensao($datasuspensao) {
        $this->datasuspensao = $datasuspensao;
    }

    public function getDatareativacao() {
        return $this->datareativacao;
    }

    public function setDatareativacao($datareativacao) {
        $this->datareativacao = $datareativacao;
    }

    
    public function getContrato() {
        return $this->contrato;
    }
    /**
     *
     * @param Application_Model_Entity_Contrato $contrato 
     */
    public function setContrato(Application_Model_Entity_Contrato $contrato) {
        $this->contrato = $contrato;
    }


}

?>
