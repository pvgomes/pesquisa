<?php
/**
 * Description of Tefone
 *
 * @author Raphael
 */
class Apllication_Model_Entity_Telefone {
    
    private $id;
    private $numero;
    private $iniciovigencia;
    private $fimvigencia;
    
    /**
     *
     * @var Application_Model_Table_Usuario 
     */
    private $usuario;
    
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function getIniciovigencia() {
        return $this->iniciovigencia;
    }

    public function setIniciovigencia($iniciovigencia) {
        $this->iniciovigencia = $iniciovigencia;
    }

    public function getFimvigencia() {
        return $this->fimvigencia;
    }

    public function setFimvigencia($fimvigencia) {
        $this->fimvigencia = $fimvigencia;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario(Application_Model_Table_Usuario $usuario) {
        $this->usuario = $usuario;
    }


    
}

?>
