<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Resposta
 *
 * @author Raphael
 */
class Application_Model_Entity_Resposta {
    private $id;
    private $respostadissertativa;
    
    

        /**
     *
     * @var Application_Model_Entity_Alternativa 
     */
    private $alternativa;
    
    /**
     *
     * @var Application_Model_Entity_Pergunta
     */
    private $pergunta;
    
    /**
     *
     * @var Application_Model_Entity_Pesquisa
     */
    private $pesquisa;
    
    /**
     *
     * @var Application_Model_Entity_Pessoa
     */
    private $pessoa;
    
    public function __construct() {
        $this->alternativa  = new Application_Model_Entity_Alternativa();
        $this->pergunta     = new Application_Model_Entity_Pergunta();        
        $this->pesquisa     = new Application_Model_Entity_Pesquisa();
        $this->pessoa       = new Application_Model_Entity_Pessoa();
    }


    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    
    public function getRespostadissertativa() {
        return $this->respostadissertativa;
    }

    public function setRespostadissertativa($respostadissertativa) {
        $this->respostadissertativa = $respostadissertativa;
    }
    
    public function getAlternativa() {
        return $this->alternativa;
    }
    /**
     *
     * @param Application_Model_Entity_Alternativa $alternativa 
     */
    public function setAlternativa(Application_Model_Entity_Alternativa $alternativa) {
        $this->alternativa = $alternativa;
    }

    
    public function getPergunta() {
        return $this->pergunta;
    }
    /**
     *
     * @param Application_Model_Entity_Pergunta $pergunta 
     */
    public function setPergunta(Application_Model_Entity_Pergunta $pergunta) {
        $this->pergunta = $pergunta;
    }

    
    public function getPesquisa() {
        return $this->pesquisa;
    }
    /**
     *
     * @param Application_Model_Entity_Pesquisa $pesquisa 
     */
    public function setPesquisa(Application_Model_Entity_Pesquisa $pesquisa) {
        $this->pesquisa = $pesquisa;
    }

    
    
    public function getPessoa() {
        return $this->pessoa;
    }
    /**
     *
     * @param Application_Model_Entity_Pessoa $pessoa 
     */
    public function setPessoa(Application_Model_Entity_Pessoa $pessoa) {
        $this->pessoa = $pessoa;
    }


}

?>
