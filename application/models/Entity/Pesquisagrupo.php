<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pesquisagrupo
 *
 * @author Raphael
 */
class Application_Model_Entity_Pesquisagrupo {
    private $id;
    
    /**
     *
     * @var Application_Model_Entity_Grupo
     */
    private $grupo;
    
    /**
     *
     * @var Application_Model_Entity_Pesquisa
     */
    private $pesquisa;
    
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getGrupo() {
        return $this->grupo;
    }

    public function setGrupo($grupo) {
        $this->grupo = $grupo;
    }

    
    public function getPesquisa() {
        return $this->pesquisa;
    }
    /**
     *
     * @param Application_Model_Entity_Pesquisa $pesquisa 
     */
    public function setPesquisa(Application_Model_Entity_Pesquisa $pesquisa) {
        $this->pesquisa = $pesquisa;
    }


}

?>
