<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Plano
 *
 * @author Raphael
 */
class Application_Model_Entity_Plano {
    private $id;
    private $nome;
    private $valor;
    private $descricao;
    private $status;
    
    /**
     *
     * @var Application_Model_Entity_Imagem
     */
    private $imagem;
    
    public function __construct(){
    	$this->imagem = new Application_Model_Entity_Imagem();
    }
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getValor() {
        return $this->valor;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    
    
    public function getImagem() {
        return $this->imagem;
    }
    /**
     *
     * @param Application_Model_Entity_Imagem $imagem 
     */
    public function setImagem(Application_Model_Entity_Imagem $imagem) {
        $this->imagem = $imagem;
    }


}

?>
