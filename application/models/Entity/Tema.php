<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tema
 *
 * @author Raphael
 */
class Application_Model_Entity_Tema {
    private $id;
    private $nomereal;
    private $nomefantasia;
    
    /**
     *
     * @var Application_Model_Entity_Imagem
     */
    private $imagem;
    
    
    public function __construct() {
        $this->imagem = new Application_Model_Entity_Imagem();
    }


    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNomereal() {
        return $this->nomereal;
    }

    public function setNomereal($nomereal) {
        $this->nomereal = $nomereal;
    }

    public function getNomefantasia() {
        return $this->nomefantasia;
    }

    public function setNomefantasia($nomefantasia) {
        $this->nomefantasia = $nomefantasia;
    }

    
    public function getImagem() {
        return $this->imagem;
    }
    /**
     *
     * @param Application_Model_Entity_Imagem $imagem 
     */
    public function setImagem(Application_Model_Entity_Imagem $imagem) {
        $this->imagem = $imagem;
    }


}

?>
