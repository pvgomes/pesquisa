<?php
/*
 * 
 * @autor Paulo Victor
 */
class Application_Model_Entity_Contrato{
	
	private $id;
	private $iniciovigencia;
	private $fimvigencia;
	
	/*
	 * @var Application_Model_Entity_Usuario
	 */
	private $usuario;
	
	/*
	 * @var Application_Model_Entity_Plano
	 */
	private $plano;
	
	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}
	
	public function getIniciovigencia(){
		return $this->iniciovigencia;
	}
	
	public function setIniciovigencia($iniciovigencia){
		$this->iniciovigencia = $iniciovigencia;
	}
	
	public function getFimvigencia(){
		return $this->fimvigencia;
	}
	
	public function setFimvigencia($fimvigencia){
		$this->fimvigencia = $fimvigencia;
	}
	
	/*
	 * @return Application_Model_Entity_Usuario
	 */
	public function getUsuario() { 
		return $this->usuario; 
	} 
	
	/*
	 * @param Application_Model_Entity_Usuario $usuario
	 */
	public function setUsuario(Application_Model_Entity_Usuario $usuario) { 
		$this->usuario = $usuario; 
	} 
	
	
	/*
	 * @return Application_Model_Entity_Plano 
	 */	
	public function getPlano() { 
		return $this->plano; 
	} 
	
	/*
	 * @return Application_Model_Entity_Plano $plano
	 */	
	public function setPlano(Application_Model_Entity_Plano $plano) { 
		$this->plano = $plano; 
	} 
	
	



}
?>