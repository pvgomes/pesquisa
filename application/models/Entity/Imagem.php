<?php

class Application_Model_Entity_Imagem {

    private $id;
    private $nome;
    private $descricao;
    private $criacao;

    /*
     * @var Application_Model_Entity_Usuario
     */
    private $usuario;

    public function __construct() {
        $this->usuario = new Application_Model_Entity_Usuario();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function getCriacao() {
        return $this->criacao;
    }

    public function setCriacao($criacao) {
        $this->criacao = $criacao;
    }

    /*
     * @return Application_Model_Entity_Usuario
     */

    public function getUsuario() {
        return $this->usuario;
    }

    /*
     * @param Application_Model_Entity_Usuario $usuario
     */

    public function setUsuario(Application_Model_Entity_Usuario $usuario) {
        $this->usuario = $usuario;
    }

}

?>