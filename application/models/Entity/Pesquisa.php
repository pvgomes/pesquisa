<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pesquisa
 *
 * @author Raphael
 */
class Application_Model_Entity_Pesquisa {
    
    private $id;
    private $nome;
    private $descricao;
    private $datainicio;
    private $datafim;
    private $url;
    private $senha;
    /**
     *
     * @var Application_Model_Entity_Formulario
     */
    private $formulario;
    /**
     *
     * @var Application_Model_Entity_Acessopesquisa 
     */
    private $acessopesquisa;
    
    public function __construct(){
        $this->formulario = new Application_Model_Entity_Formulario();
        $this->acessopesquisa = new Application_Model_Entity_Acessopesquisa();
    }
    
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function getDatainicio() {
        return $this->datainicio;
    }

    public function setDatainicio($datainicio) {
        $this->datainicio = $datainicio;
    }

    public function getDatafim() {
        return $this->datafim;
    }

    public function setDatafim($datafim) {
        $this->datafim = $datafim;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    
    public function getFormulario() {
        return $this->formulario;
    }
    
    public function setSenha($senha){
        $this->senha = $senha;
    }
    
    public function getSenha(){
        return $this->senha;
    }
    
    /**
     *
     * @param Application_Model_Entity_Formulario $formulario 
     */
    public function setFormulario(Application_Model_Entity_Formulario $formulario) {
        $this->formulario = $formulario;
    }

    
    public function getAcessopesquisa() {
        return $this->acessopesquisa;
    }
    /**
     *
     * @param Application_Model_Entity_Acessopesquisa $acessopesquisa 
     */
    public function setAcessopesquisa(Application_Model_Entity_Acessopesquisa $acessopesquisa) {
        $this->acessopesquisa = $acessopesquisa;
    }


    
    
    
}

?>
