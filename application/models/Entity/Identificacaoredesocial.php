<?php

class Application_Model_Entity_Identificacaoredesocial{

	private $id;
	private $identificacao;

	/*
	 * @var Application_Model_Entity_Redesocial
	 */
	private $redesocial;

	/*
	 * @var Application_Model_Entity_Pessoa
	 */
	private $pessoa;



	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}



	public function getIdentificacao() {
		return $this->identificacao;
	}

	public function setIdentificacao($identificacao) {
		$this->identificacao = $identificacao;
	}


	/*
	 * @return Application_Model_Entity_Redesocial
	 */
	public function getRedesocial() {
		return $this->redesocial;
	}

	/*
	 * @param Application_Model_Entity_Redesocial $redesocial
	 */
	public function setRedesocial(Application_Model_Entity_Redesocial $redesocial) {
		$this->redesocial = $redesocial;
	}

	/*
	 * @return Application_Model_Entity_Pessoa
	 */
	public function getPessoa() {
		return $this->pessoa;
	}

	/*
	 * @return Application_Model_Entity_Pessoa $pessoa
	 */
	public function setPessoa(Application_Model_Entity_Pessoa $pessoa) {
		$this->pessoa = $pessoa;
	}



}
?>