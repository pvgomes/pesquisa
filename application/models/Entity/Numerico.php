<?php
class Application_Model_Entity_Numerico{
	
	private $id;
	private $numresposta;
	
	/*
	 * @var Application_Model_Entity_Resposta
	 */
	private $resposta;

	public function getId() { 
		return $this->id; 
	} 
	
	public function setId($id) { 
		$this->id = $id; 
	} 
	
	public function getNumresposta() { 
		return $this->numresposta; 
	} 
	
	public function setNumresposta($numresposta) { 
		$this->numresposta = $numresposta; 
	} 
		
	/*
	 * @return Application_Model_Entity_Resposta
	 */
	public function getResposta() { 
		return $this->resposta; 
	} 
	
	/*
	 * @return Application_Model_Entity_Resposta $resposta
	 */
	public function setResposta(Application_Model_Entity_Resposta $resposta) { 
		$this->resposta = $resposta; 
	} 
	
}
?>