<?php
/*
 *  @autor Paulo Victor 
 */

class Application_Model_Entity_Convite{
	
	private $id;
	private $status;
	private $data;
	
	/*
	 * 
	 * @var Application_Model_Entity_Usuario
	 */
	private $usuario;
	
	/*
	 * 
	 * @var Application_Model_Entity_Pessoa
	 */
	private $pessoa;
	
	public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
	
    public function getStatus(){
    	return $this->status;
    }
    
    public function setStatus($status){
    	$this->status = $status;
    }
    
    public function setData(){
    	return $this->data;
    }
    
    public function getData($data){
    	$this->data = $data;
    }
    
    /*
     * 
     * @return Application_Model_Entity_Usuario
     */
    public function getUsuario(){
    	return $this->usuario;
    }
    
    /*
     * 
     * @param Application_Model_Entity_Usuario $usuario
     */
    public function setUsuario(Application_Model_Entity_Usuario $usuario){
    	$this->usuario = $usuario; 	
    }
    
    /*
     * 
     * @return Application_Model_Entity_Pessoa
     */
	public function getPessoa(){
		return $this->pessoa;
	}
	
	/*
	 * 
	 * @param Application_Model_Entity_Pessoa @pessoa
	 */
	public function setPessoa(Application_Model_Entity_Pessoa $pessoa){
		$this->pessoa = $pessoa;
	}
}

?>