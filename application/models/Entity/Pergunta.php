<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pergunta
 *
 * @author Raphael
 */
class Application_Model_Entity_Pergunta {

    private $id;
    private $descricao;
    private $predefinida;

    /**
     *
     * @var Application_Model_Entity_Tipopergunta
     */
    private $tipopergunta;

    /**
     *
     * @var Application_Model_Entity_Formulario
     */
    private $formulario;

    /**
     *
     * @var Application_Model_Entity_Imagem
     */
    private $imagem;
   
    /*
     * Array de alternativas
     */
    private $alternativas;

    public function __construct() {
        $this->alternativas = array();
        $this->formulario = new Application_Model_Entity_Formulario();
        $this->tipopergunta = new Application_Model_Entity_Tipopergunta();
    }
    
    public function getAlternativas(){
        return $this->alternativas;
    }
    
    public function addAlternativas($alternativa){
        array_push($this->alternativas,$alternativa);
    }
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function getPredefinida() {
        return $this->predefinida;
    }

    public function setPredefinida($predefinida) {
        $this->predefinida = $predefinida;
    }

    public function getTipopergunta() {
        return $this->tipopergunta;
    }

    /**
     *
     * @param Application_Model_Entity_Tipopergunta $tipopergunta 
     */
    public function setTipopergunta(Application_Model_Entity_Tipopergunta $tipopergunta) {
        $this->tipopergunta = $tipopergunta;
    }

    public function getFormulario() {
        return $this->formulario;
    }

    /**
     *
     * @param Application_Model_Entity_Formulario $formulario 
     */
    public function setFormulario(Application_Model_Entity_Formulario $formulario) {
        $this->formulario = $formulario;
    }

    public function getImagem() {
        return $this->imagem;
    }

    /**
     *
     * @param Application_Model_Entity_Imagem $imagem 
     */
    public function setImagem(Application_Model_Entity_Imagem $imagem) {
        $this->imagem = $imagem;
    }

}

?>
