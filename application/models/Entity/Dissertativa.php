<?php
/**
 * Dissertativa
 * @author paulo
 *
 */
class Application_Model_Entity_Dissertativa{

	private $id;
	private $descricao;

	/*
	 *
	 * @var Application_Model_Entity_Resposta
	 */
	private $resposta;


    public function getId() {
	return $this->id; 
    }
    
	public function setId($id) {
		$this->id = $id;
	}

	public function getDescricao() {
		return $this->descricao;
	}
	
	public function setDescricao($descricao) {
		$this->descricao = $descricao;
	}


	/*
	 * 
	 * @return Application_Model_Entity_Resposta
	 */
	public function getResposta() {
		return $this->resposta;
	}

	/*
	 * 
	 * @param Application_Model_Entity_Resposta $resposta
	 */
	public function setResposta(Application_Model_Entity_Resposta $resposta)
	{ 
		$this->resposta = $resposta;
	}
	
}

?>