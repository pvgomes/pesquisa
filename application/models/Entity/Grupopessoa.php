<?php
class Application_Model_Entity_Grupopessoa{

	private $id;
	
	/*
	 * @var Application_Model_Entity_Pessoa
	 */
	private $pessoa;
	
	/*
	 * 
	 * @var Application_Model_Entity_Grupo
	 */
	private $grupo;
	
	public function getId() { 
		return $this->id; 
	} 
	
	public function setId($id) {
		$this->id = $id; 
	} 
	
	/*
	 * 
	 * @return Application_Model_Entity_Pessoa
	 */
	
	public function getPessoa() {
		return $this->pessoa; 
	} 
	
	/*
	 * 
	 * @param Application_Model_Entity_Pessoa $pessoa
	 */
	public function setPessoa(Application_Model_Entity_Pessoa $pessoa) { 
		$this->pessoa = $pessoa; 
	} 
	
	
	/*
	 * 
	 * @return Application_Model_Entity_Grupo
	 */
	public function getGrupo() { 
		return $this->grupo; 
	} 
	
	/*
	 * 
	 * @param Application_Model_Entity_Grupo $grupo
	 */
	
	public function setGrupo(Application_Model_Entity_Grupo $grupo) { 
		$this->grupo = $grupo; 
	} 
	
	
}

?>