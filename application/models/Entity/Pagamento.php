<?php
class Application_Model_Entity_Pagamento{

	private $id;
	private $datapagamento;
	private $vencimento;
	private $valorcobrado;
	private $valorpago;
	private $juros;

	/*
	 * @var Application_Model_Entity_Contrato
	 */
	private $contrato;


	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
	}

	public function getDatapagamento() {
		return $this->datapagamento;
	}
	public function setDatapagamento($datapagamento) {
		$this->datapagamento = $datapagamento;
	}

	public function getVencimento() {
		return $this->vencimento;
	}
	public function setVencimento($vencimento) {
		$this->vencimento = $vencimento;
	}

	public function getValorcobrado() {
		return $this->valorcobrado;
	}
	public function setValorcobrado($valorcobrado) {
		$this->valorcobrado = $valorcobrado;
	}

	public function getValorpago() {
		return $this->valorpago;
	}
	public function setValorpago($valorpago) {
		$this->valorpago = $valorpago;
	}
	

	public function getJuros() {
		return $this->juros;
	}

	public function setJuros($juros) {
		$this->juros = $juros;
	}

	/*
	 * @return Application_Model_Entity_Contrato
	 */
	public function getContrato() {
		return $this->juros;
	}

	public function setContrato(Application_Model_Entity_Contrato $contrato) {
		$this->juros = $contrato;
	}
	
}