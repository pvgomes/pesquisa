<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Participantes
 *
 * @author Raphael
 */
class Application_Model_Entity_Participantes {
    
    private $id;
    /**
     *
     * @var Application_Models_Entity_Pesquisa
     */
    private $pesquisa;
    
    /**
     *
     * @var Application_Models_Entity_Pessoa
     */
    private $pessoa;
    
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    
    public function getPesquisa() {
        return $this->pesquisa;
    }
    /**
     *
     * @param Application_Models_Entity_Pesquisa $pesquisa 
     */   
    public function setPesquisa(Application_Models_Entity_Pesquisa $pesquisa) {
        $this->pesquisa = $pesquisa;
    }

    
    
    public function getPessoa() {
        return $this->pessoa;
    }
    /**
     *
     * @param Application_Models_Entity_Pessoa $pessoa 
     */
    public function setPessoa(Application_Models_Entity_Pessoa $pessoa) {
        $this->pessoa = $pessoa;
    }


    
    
    
}

?>
