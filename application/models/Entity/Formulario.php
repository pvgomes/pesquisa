<?php

class Application_Model_Entity_Formulario {

    private $id;
    private $nome;
    private $descricao;
    /*
     * @var Application_Model_Entity_Usuario
     */
    private $usuario;

    /*
     * @var Application_Model_Entity_Tema
     */
    private $tema;

    /**
     *
     * @var Application_Model_Entity_Pergunta
     */
    private $pergunta;

    public function __construct() {
        $this->pergunta = array();
        $this->usuario = new Application_Model_Entity_Usuario();
        $this->tema = new Application_Model_Entity_Tema();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    /*
     * 
     * @return Application_Model_Entity_Usuario
     */

    public function getUsuario() {
        return $this->usuario;
    }

    /*
     * 
     * @return Application_Model_Entity_Usuario $usuario
     */

    public function setUsuario(Application_Model_Entity_Usuario $usuario) {
        $this->usuario = $usuario;
    }

    /*
     * 
     * @return Application_Model_Entity_Tema
     */

    public function getTema() {
        return $this->tema;
    }

    /*
     * 
     * @return Application_Model_Entity_Tema $tema
     */

    public function setTema(Application_Model_Entity_Tema $tema) {
        $this->tema = $tema;
    }

    public function getPergunta() {
        return $this->pergunta;
    }

    /**
     *
     * @param Application_Model_Entity_Pergunta $pergunta 
     */
    public function addPergunta(Application_Model_Entity_Pergunta $pergunta) {
        array_push($this->pergunta, $pergunta);
    }

}

?>