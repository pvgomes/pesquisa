<?php

class Application_Model_Entity_Limiteplano{

	private $id;
	private $valor;
	
	/*
	 * @var Application_Model_Entity_Tipolimite
	 */
	private $tipolimite;
	
	/*
	 * @var Application_Model_Entity_Plano
	 */
	private $plano;
	
	
	public function getId() { 
		return $this->id; 
	} 
	public function setId($id) { 
		$this->id = $id; 
	} 
	
	
	public function getValor() { 
		return $this->valor; 
	} 
	public function setValor($valor) { 
		$this->valor = $valor; 
	} 
	
	
	/*
	 * @return Application_Model_Entity_Tipolimite
	 */
	public function getTipolimite() { 
		return $this->tipolimite; 
	} 
	
	/*
	 * @param Application_Model_Entity_Tipolimite $tipolimite
	 */
	public function setTipolimite(Application_Model_Entity_Tipolimite $tipolimite) { 
		$this->tipolimite = $tipolimite; 
	}
}

?>