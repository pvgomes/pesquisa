<?php
/**
 * Description of Endereco
 *
 * @author Raphael
 */
class Apllication_Model_Entity_Endereco {
    
    private $id;
    private $logradouro;
    private $bairro;
    private $numero;
    private $estado;
    private $cidade;
    private $cep;
    private $iniciovigencia;
    private $fimvigencia;
    private $sequencia;
    
    /**
     *
     * @var Application_Model_Table_Usuario
     */
    private $usuario;
    
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getLogradouro() {
        return $this->logradouro;
    }

    public function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
    }

    public function getBairro() {
        return $this->bairro;
    }

    public function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    public function getCep() {
        return $this->cep;
    }

    public function setCep($cep) {
        $this->cep = $cep;
    }

    public function getIniciovigencia() {
        return $this->iniciovigencia;
    }

    public function setIniciovigencia($iniciovigencia) {
        $this->iniciovigencia = $iniciovigencia;
    }

    public function getFimvigencia() {
        return $this->fimvigencia;
    }

    public function setFimvigencia($fimvigencia) {
        $this->fimvigencia = $fimvigencia;
    }

    public function getSequencia() {
        return $this->sequencia;
    }

    public function setSequencia($sequencia) {
        $this->sequencia = $sequencia;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario(Application_Model_Table_Usuario $usuario) {
        $this->usuario = $usuario;
    }


}

?>
