<?php
/**
 * Description of Participantes
 *
 * @author Paulo Victor
 */
class Application_Model_Entity_Alternativa{
	
	private $id;
	private $descricao;
	
	/**
	 * @var Application_Model_Entity_Pergunta
	 */
	private $pergunta;
	
	/**
	 * @var Application_Model_Entity_Imagem
	 */
	private $imagem;
	
        public function __construct() {
            $this->pergunta = new Application_Model_Entity_Pergunta();
            //$this->imagem = new Application_Model_Entity_Imagem();
        }
        
	public function getId(){
		return $this->id;
	}
	public function setId($id){
		$this->id = $id;
	}
	
	public function getDescricao(){
		return $this->descricao;
	}
	
	public function setDescricao($descricao){
		$this->descricao = $descricao;
	}
	
	
	/**
	 * @return Application_Model_Entity_Pergunta
	 */
	public function getPergunta(){
		return $this->pergunta;
	}
	
	/**
	 * 
	 * @param Application_Model_Entity_Pergunta $pergunta
	 */
	public function setPergunta(Application_Model_Entity_Pergunta $pergunta){
		$this->pergunta = $pergunta;		
	}
	
	/**
	* @return Application_Model_Entity_Imagem
	*/
	public function getImagem(){
		return $this->imagem;
	}
	
	/**
	* @param Application_Model_Entity_Imagem $imagem
	*/
	public function setImagem(Application_Model_Entity_Imagem $imagem){
		$this->imagem = $imagem;
	}
}
?>