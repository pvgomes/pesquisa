<?php
/**
 * Controller para configurações de usuarios
 */
class ConfiguracaoController extends Zend_Controller_Action {

    private $request;
    private $dao;
    private $entity;

    public function init() {
        $this->request = $this->getRequest();
    }
    
    public function indexAction(){
        $apelido = Zend_Auth::getInstance()->getStorage()->read()->usu_id;
        $this->dao = new Application_Model_Dao_Usuario();
        $this->dao->buscaTudo($apelido);
    }

}