<?php

class RelatorioController extends Zend_Controller_Action {

    private $dao;
    private $usuario;
    private $request;

    public function init() {
        $this->request = $this->getRequest();
    }

    public function indexAction() {
        
        $idPesquisa = $this->request->getParam("id");
        
        $this->dao = new Application_Model_Dao_Pesquisa();
        $this->view->pesquisa = $this->dao->buscaPesquisaPorUsuario(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
        $this->view->resposta = $this->dao->respostaPorPesquisa($idPesquisa);
        $this->view->id = $idPesquisa;
    }

}

?>