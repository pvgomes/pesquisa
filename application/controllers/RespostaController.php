<?php

class RespostaController extends Zend_Controller_Action {

    private $request;
    private $dao;
    private $entity;

    public function init() {
        $this->request = $this->getRequest();
    }

    public function indexAction() {
        
    }

    /*
     * Questionario recebe um id da pesquisa
     * Coleta dados e valida usuahg branch-a-cal
     * rio que vai responder
     * Filtra respostas
     */

    public function questionarioAction() {
        /**
         * PRIMEIRO É PRECISO BUSCAR A PESQUISA PARA SABER VALIDAÇÕES E ETC
         */
        $this->dao = new Application_Model_Dao_Pesquisa();
        $this->pesquisa = $this->dao->buscaPesquisaId($this->request->getParam('peq'));
        $this->view->pesquisa = $this->pesquisa;
    }

    /**
     * Valida é a action que valida o acesso a pesquisa
     * Recebe uma senha e o id da pesquisa
     */
    public function validaAction() {
        $idpesquisa = $this->request->getParam('idpesquisa');
        $senha = $this->request->getParam('senha');

        $this->dao = new Application_Model_Dao_Pesquisa();
        $this->pesquisa = $this->dao->buscaPesquisaId($idpesquisa);

        /*
         * Se existir senha então precisamos validar
         * Se não, retorna verdadeiro e deixa passar
         */
        if ($this->pesquisa->getSenha()) {
            /**
             * Se for igual a senha do usuario então valida
             */
            if ($senha == $this->pesquisa->getSenha()):
                echo "1";
            endif;
        }else {
            echo "1";
        }

        exit;
    }

    /**
     * Responde é a action que retorna todas as respostas para responder
     * Recebe um formulario como parametro
     */
    public function responderAction() {
        $idformulario = $this->request->getParam('id');
        $idpesquisa = $this->request->getParam('idpesquisa');


        $this->dao = new Application_Model_Dao_Pergunta();
        $this->view->perguntas = $this->dao->buscarPorFormulario($idformulario);

        $this->view->idformulario = $idformulario;
        $this->view->idpesquisa = $idpesquisa;
    }

    /**
     * evetivaresposta é a action que envia as respostas
     * Recebe o formulario completo
     */
    public function evetivarespostaAction() {
        $idpesquisa = $this->request->getParam('idpesquisa');
        $idformulario = $this->request->getParam('idformulario');
        $idpessoa = Zend_Auth::getInstance()->getStorage()->read()->usu_id;


        /**
         * Recebe as alternativas
         */
        //SE TIVER PASSANDO PERGUNTA COM ALTERNATIVA
        if ($this->request->getParam('pergunta')) {
            foreach ($this->request->getParam('pergunta') as $i => $r):
                //echo "<br><br>" . $i . " - " . $r . "<br><br>";


                $this->entity = new Application_Model_Entity_Resposta();
                $this->entity->getAlternativa()->setId($r);
                $this->entity->getPesquisa()->setId($idpesquisa);
                $this->entity->getPergunta()->setId($i);
                $this->entity->getPessoa()->setId($idpessoa);

                $this->dao = new Application_Model_Dao_Resposta();
                $this->dao->inserir($this->entity);


            endforeach;
        }


        /**
         * Recebe as dissertativas
         * dissertativa
         */
        //SE TIVER PASSANDO PERGUNTA DISSERTATIVA
        if ($this->request->getParam('dissertativa')) {
            foreach ($this->request->getParam('dissertativa') as $i => $r):
                //echo "<br><br>" . $i . " - " . $r . "<br><br>";

                $this->entity = new Application_Model_Entity_Resposta();
                $this->entity->getPesquisa()->setId($idpesquisa);
                $this->entity->getPergunta()->setId($i);
                $this->entity->setRespostadissertativa($r);
                $this->entity->getPessoa()->setId($idpessoa);

                $this->dao = new Application_Model_Dao_Resposta();

                $this->dao->inserir($this->entity);



            //var_dump($this->entity);
            endforeach;
        }
    }

}