<?php

class UsuarioController extends Zend_Controller_Action {

    private $request;
    private $dao;
    private $entity;

    public function init() {
        $this->request = $this->getRequest();
    }

    public function indexAction() {
        
    }

    public function formularioAction() {
        //CASO TENHA REQUISITAÇÃO NESSE MESMO ACTION
        if ($this->getRequest()->isPost()) {

            $this->formulario = new Application_Model_Entity_Formulario();
            $this->formulario->setNome($this->request->getParam('nome'));
            $this->formulario->setDescricao($this->request->getParam('descricao'));
            $this->formulario->getUsuario()->setId(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
            $this->formulario->getTema()->setId($this->request->getParam('tema'));

            $this->dao = new Application_Model_Dao_Formulario();
            $id = $this->dao->inserir($this->formulario);


            //verifica se ouve retorno
            if ($id != null) {
                $this->_redirect('/usuario/pergunta/id/' . $id);
                //$this->view->idform = $id;
            }
        }

        $this->dao = new Application_Model_Dao_Tema();
        $this->view->listaTema = $this->dao->buscaTema();

        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->modeloFormulario = $this->dao->buscaFormularioModelo();

        $this->view->meusModelos = $this->dao->buscarFormularios(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
    }

    public function perguntaAction() {

        if ($this->getRequest()->isPost()) {

            $this->entity = new Application_Model_Entity_Pergunta();
            $this->entity->setDescricao($this->request->getParam('pergunta'));
            $this->entity->getFormulario()->setId($this->request->getParam('id'));
            //FUTURAMENTE DAR POSSIBILIDADE DE COLOCAR IMAGEM

            if ($this->request->getParam('tipo') == 'alternativa') {
                //ALTERNATIVA
                $this->entity->getTipopergunta()->setId(1);
                $dados = $this->request->getParam('alternativa');
            } else {
                //DISSERTATIVA
                $this->entity->getTipopergunta()->setId(2);
            }
            $this->dao = new Application_Model_Dao_Pergunta();
            $idPergunta = $this->dao->novaPergunta($this->entity);

            $this->dao = new Application_Model_Dao_Alternativa();
            $this->entity = new Application_Model_Entity_Alternativa();

            if ($this->request->getParam('tipo') == 'alternativa') {
                foreach ($dados as $row):
                    $this->entity->getPergunta()->setId($idPergunta);
                    $this->entity->setDescricao($row);
                    $this->dao->inserir($this->entity);
                endforeach;
            }
        }
        //Mostra tipos de pergunta
        $this->dao = new Application_Model_Dao_Tipopergunta();
        $this->view->tipoPergunta = $this->dao->buscaTipos();

        $this->dao = new Application_Model_Dao_Pergunta();
        $this->view->perguntas = $this->dao->buscarPorFormulario($this->request->getParam('id'));

        //BUSCA FORMULARIO POR ID
        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->formulario = $this->dao->buscarUmFormulario($this->request->getParam('id'));

        //PASSA ID FO FORMULARIO
        $this->view->id = $this->request->getParam('id');
    }

    public function paginasAction() {
        
    }

    public function pesquisaAction() {
        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->listaFormulario = $this->dao->buscarFormularios(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
    }

    public function relatorioAction() {
        
        $id = Zend_Auth::getInstance()->getStorage()->read()->usu_id;
        
        $this->dao = new Application_Model_Dao_Pesquisa();
        $this->view->pesquisa = $this->dao->buscaPesquisaPorUsuario(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
        
    }

    public function configuracaoAction() {
        
    }

    public function excluirformAction() {
        $id = $this->request->getParam('id');

        $formulario = new Application_Model_Dao_Formulario();
        echo $formulario->deletar($id);

        exit;
    }

    //SERVE PARA NOVA PESQUISA E PARA EDITAR A PESQUISA
    public function novapesquisaAction() {
        $this->view->idformulario = $this->request->getParam('id');
        $pesquisa = $this->request->getParam('peq');
        if ($pesquisa) {
            $this->dao = new Application_Model_Dao_Pesquisa();
            $this->view->pesquisa = $this->dao->buscaPesquisaId($pesquisa, Zend_Auth::getInstance()->getStorage()->read()->usu_id);
        } else {
            $this->entity = new Application_Model_Entity_Pesquisa();
            $this->view->pesquisa = $this->entity;
        }


        if ($this->getRequest()->isPost()) {

            $this->entity = new Application_Model_Entity_Pesquisa();

            if ($this->request->getParam('idpesquisa')) {
                $this->entity->setId($this->request->getParam('idpesquisa'));
            }

            $this->entity->setNome($this->request->getParam('nome'));
            $this->entity->setDescricao($this->request->getParam('descricao'));

            $this->entity->getFormulario()->setId($this->request->getParam('idformulario'));

            if ($this->request->getParam('datainicio')) {
                $datainicio = $this->_helper->getHelper('Data')->data2sql($this->request->getParam('datainicio'));
                $datafim = $this->_helper->getHelper('Data')->data2sql($this->request->getParam('datafim'));
                $this->entity->setDatainicio($datainicio);
                $this->entity->setDatafim($datafim);
            }

            $this->entity->getAcessopesquisa()->setId($this->request->getParam('acesso'));
            //SE FOR FECHADO PEGA A SENHA 
            if ($this->request->getParam('acesso') == 2) {
                //$this->request->getParam('senha');
                $this->entity->setSenha($this->request->getParam('senha'));
            } else {
                $this->entity->setSenha(null);
            }

            $this->dao = new Application_Model_Dao_Pesquisa();
            //SE EXISTIR ID MANDA PARA ATUALIZAÇÃO
            if ($this->entity->getId()) {

                $this->dao->atualiza($this->entity, $this->request->getParam('idpesquisa'));
                $this->_redirect('/usuario/verpesquisa/id/' . $this->entity->getId());
            }


            $id = $this->dao->nova($this->entity);
            if ($id) {
                $this->dao->montaurl($id);
                $this->_redirect('/usuario/verpesquisa/id/' . $id);
            } else {
                echo "ERRO AO APLICAR UMA PESQUISA";
                die;
            }
        }
    }

    public function verpesquisaAction() {
        /*
         * @param id da pesquisa 
         */
        $this->dao = new Application_Model_Dao_Pesquisa();
        $this->view->pesquisa = $this->dao->buscaPesquisaId($this->request->getParam('id'), Zend_Auth::getInstance()->getStorage()->read()->usu_id);
    }

    public function gerarsenhaAction() {
        //echo md5(date('ymdhi'));
        echo rand();
        exit;
    }

    public function verformularioAction() {
        $id = $this->request->getParam('id');

        $this->dao = new Application_Model_Dao_Pesquisa();
        $this->view->ver = $this->dao->buscaPesquisaPorFormulario($id);
    }

    public function modeloAction() {
        
        $id = $this->request->getParam('id');
        
        $this->dao = new Application_Model_Dao_Formulario();
        $this->dao->modelo($id);       
    }

}