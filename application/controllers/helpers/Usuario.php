<?php

class Zend_Controller_Action_Helper_Usuario extends Zend_Controller_Action_Helper_Abstract {

    public function login($usuario, $senha) {
        /**
         * Instancia o Auth Db Table Adapter
         *
         * Quando se instancia este objeto, precisamos informar as configura��es
         * do BD, nome da tabela onde os dados de login estão, o campo do nome
         * do usuario, e o campo da senha na tabela.
         */
        $dbAdapter = Zend_Registry::get('db');

        $adapter = new Zend_Auth_Adapter_DbTable(
                        $dbAdapter,
                        'usu_usuario',
                        'usu_usuario',
                        'usu_senha'
        );

        // Configura as credencias user_email e user_password informadas pelo usu�rio
        $adapter->setIdentity($usuario);
        $adapter->setCredential($senha);

        // Cria uma instancia de Zend_Auth
        $auth = Zend_Auth::getInstance();

        // Tenta autenticar o usu�rio
        $result = $auth->authenticate($adapter);

        /**
         * Se o usu�rio for autenticado redireciona para a index e grava seu email,
         * caso contr�rio exibe uma mensagem de alerta na p�gina
         */
        if ($result->isValid()) {
            $data = $adapter->getResultRowObject((array('usu_id', 'usu_usuario', 'usu_senha', 'pes_id', 'tus_id')));

            // Armazena os dados do usuario
            $auth->getStorage()->write($data);
            return true;
        }else{
            return false;
        }
    }

    public function logout($string) {
        Zend_Auth::getInstance()->clearIdentity();
    }

}

?>