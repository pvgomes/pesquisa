<?php

class Zend_Controller_Action_Helper_Data extends Zend_Controller_Action_Helper_Abstract {
    /*
     * @param uma data
     * @return data formato sql
     */

    public function data2sql($data) {

        if ($data == "") {
            return null;
        } else {

            $ano = substr($data, 6, 4);
            $mes = substr($data, 3, 2);
            $dia = substr($data, 0, 2);
            return ($ano . "-" . $mes . "-" . $dia);
        }
    }

    public function sql2data($data) {
        
    }

}

?>