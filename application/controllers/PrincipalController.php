<?php

//error_reporting(0);

class PrincipalController extends Zend_Controller_Action {

    private $request;

    public function init() {
        $this->request = $this->getRequest();
    }

    public function indexAction() {
        //instancia o formulario de login
        $form = new Application_Form_Login();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {

                $this->_helper->getHelper('Usuario')->login($form->getValue('txtUserName'), $form->getValue('txtPassword'));


                if (Zend_Auth::getInstance()->getStorage()->read()->tus_id == 3) {
                    $this->_redirect('/admin/index');
                } else if (Zend_Auth::getInstance()->getStorage()->read()->tus_id == 2) {
                    $this->_redirect('/usuario/index');
                } else if (Zend_Auth::getInstance()->getStorage()->read()->tus_id == 1) {
                    $this->_redirect('/usuario/index');
                } else {
                    $this->view->message = 'Usuario/senha invalidos';
                    //echo "erro"; exit;
                }
            }
        }
        $this->view->form = $form;
    }

    function logoutAction() {

        $this->_helper->getHelper('Usuario')->logout();

        $this->_redirect('/principal/index');
    }

    function sobreAction() {
        
    }

    function contatoAction() {
        
    }

    function pesquisaAction() {
        
    }

    function restritoAction() {
        
    }

    function adminAction() {

        $this->plano = new Application_Model_Dao_Plano();
        $this->view->ativos = $this->plano->buscarTodos();

        $this->view->desativados = $this->plano->buscarDesativados();
    }

    function formularioAction() {
        
    }

    function cadastroAction() {
        $form = new Application_Form_Cadastro();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {


                echo $this->getRequest()->getParam('nome');
                die;
            }
        }

        $this->view->form = $form;
    }

    public static function convetSql($data) {

        if ($data == "") {
            return null;
        } else {

            $ano = substr($data, 6, 4);
            $mes = substr($data, 3, 2);
            $dia = substr($data, 0, 2);
            return ($ano . "-" . $mes . "-" . $dia);
        }
    }

    public function verificausuarioAction() {
        // ACTION Q VERIFICA DUPLICIDADE DE USUARIO 
        $usuario = $this->request->getParam('usuario');
        $this->dao = new Application_Model_Dao_Usuario();

        //echo $this->dao->buscarUsuario($usuario);
        //exit;

        if ($this->dao->buscarUsuario($usuario)) {
            echo 1;
        }
        exit;
    }

    public function verificaemailAction() {
        $email = $this->request->getParam('email');

        $this->dao = new Application_Model_Dao_Pessoa();

        if ($this->dao->buscaEmail($email)) {
            echo 1;
        }
        exit;
    }

    public function cadastrarAction() {

        $this->usu = new Application_Model_Entity_Pessoa();
        $this->usu->setEmail($this->request->getParam('email'));
        $this->usu->setNome($this->request->getParam('nome'));

        $this->usu->setDatanascimento($this->convetSql($this->request->getParam('nascimento')));

        $this->dao = new Application_Model_Dao_Pessoa();
        $id = $this->dao->inserir($this->usu);

        $this->usu = new Application_Model_Entity_Usuario();
        $this->usu->setUsuario($this->request->getParam('usuario'));
        $this->usu->setSenha($this->request->getParam('senha'));
        $this->usu->getPessoa()->setId($id);

        $this->dao = new Application_Model_Dao_Usuario();
        $id = $this->dao->inserir($this->usu);

        if ($id != null) {
            echo 1;
        }
        exit;
    }

}
