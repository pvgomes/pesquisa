<?php

class AdminController extends Zend_Controller_Action {

    private $dao;
    private $usuario;
    private $request;

    public function init() {
        $this->request = $this->getRequest();
    }

    public function indexAction() {


        //busca a quantidade de modelos de formularios
        $this->formulario = new Application_Model_Dao_Formulario();
        $this->view->quantidadeFormulario = $this->formulario->countModelo();

        //busca a quantidade de planos
        $this->plano = new Application_Model_Dao_Plano();
        $this->view->quantidadePlano = $this->plano->countPlanos();

        //busca a quantidade de usuarios
        $this->usuario = new Application_Model_Dao_Usuario();
        $this->view->quantidadeUsuario = $this->usuario->countUsuario();

        //busca a quantidade de pesquisa em andamento
        $this->pesquisa = new Application_Model_Dao_Pesquisa();
        $this->view->quantidadePesquisa = $this->pesquisa->countPesquisa();

        $this->plano = new Application_Model_Dao_Plano();
        $this->view->ativos = $this->plano->buscarTodos();

        $this->view->desativados = $this->plano->buscarDesativados();
    }

    public function configuracaoAction() {
        $this->dao = new Application_Model_Dao_Usuario();
        $this->dao->buscarUsuario(Zend_Auth::getInstance()->getStorage()->read()->usu_id);

    }

    public function formularioAction() {

        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->listaFormulario = $this->dao->buscarFormularios();

        $this->dao = new Application_Model_Dao_Tema();
        $this->view->listaTema = $this->dao->buscaTema();

        $this->dao = new Application_Model_Dao_Pergunta();
        $this->view->quantidadePergunta = $this->dao->buscaQuantidadePergunta();
    }

    public function alterarsenhaAction() {


        $this->usuario = new Application_Model_Entity_Usuario();
        $this->usuario->setSenha($this->request->getParam('senhanova'));

        $this->dao = new Application_Model_Dao_Usuario();
        $this->dao->novaSenha($this->usuario->getSenha(), Zend_Auth::getInstance()->getStorage()->read()->usu_id);

        $usuario = Zend_Auth::getInstance()->getStorage()->read()->usu_usuario;

        //$this->_helper->getHelper('Usuario')->logout();
        //$this->_helper->getHelper('Usuario')->login($usuario, $this->request->getParam('senhanova'));

        echo "1";
        exit;
    }

    public function altstatusAction() {

        $this->dao = new Application_Model_Dao_Plano();
        $this->dao->altstatus($this->request->getParam('id'), $this->request->getParam('status'));

        $this->_redirect('/admin/index');
    }

    public function novoAction() {
        $upload_adapter = new Zend_File_Transfer_Adapter_Http();
        //FORMA NOVA, CARREGANDO NA PUBLIC
        $upload_adapter->setDestination('imagens/planos');

        //CARREGA NO SERVIDOR
        $upload_adapter->receive();

        //var_dump($upload_adapter); exit;
        $imagem = basename($upload_adapter->getFileName());


        //estanciar a imagem, pois o plano precisa ter uma imagem
        //por isso estamos adicionando a imagem
        $this->imagem = new Application_Model_Entity_Imagem();
        $this->imagem->setCriacao(date('Y-m-d'));
        $this->imagem->setDescricao('Imagem de plano');
        $this->imagem->setNome($imagem);
        $this->imagem->getUsuario()->setId(Zend_Auth::getInstance()->getStorage()->read()->usu_id);

        $this->dao = new Application_Model_Dao_Imagem();
        $idImagem = $this->dao->inserir($this->imagem);

        $this->plano = new Application_Model_Entity_Plano();
        $this->plano->setNome($this->request->getParam('nome'));
        $this->plano->setValor($this->request->getParam('valor'));
        $this->plano->setDescricao($this->request->getParam('descricao'));
        $this->plano->getImagem()->setId($idImagem);


        $this->dao = new Application_Model_Dao_Plano();
        $id = $this->dao->inserir($this->plano);

        $this->_redirect('/admin/index');
    }

    public function excluirformAction() {
        
        $id = $this->request->getParam('id');

        $this->formulario = new Application_Model_Dao_Formulario();

        echo $this->formulario->deletar($id);


        exit;
    }

    public function editarformAction() {
        $id         = $this->request->getParam('id');
        $nome       = $this->request->getParam('nome');
        $descricao  = $this->request->getParam('descricao');

        $this->formulario = new Application_Model_Dao_Formulario();

        echo $this->formulario->alterar($id, $nome, $descricao);

        echo "1";

        exit;
    }

    public function novoformularioAction() {

        $this->formulario = new Application_Model_Entity_Formulario();
        $this->formulario->setNome($this->request->getParam('nome'));
        $this->formulario->setDescricao($this->request->getParam('descricao'));
        $this->formulario->getUsuario()->setId(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
        $this->formulario->getTema()->setId($this->request->getParam('tema'));

        $this->dao = new Application_Model_Dao_Formulario();
        $idform = $this->dao->inserir($this->formulario);


        //verifica se ouve retorno
        if ($idform != null) {
            $this->_redirect('/admin/pergunta/id/' . $idform);
            //$this->view->idform = $id;
        }
    }
    
    public function perguntaAction() {

        if ($this->getRequest()->isPost()) {

            $this->entity = new Application_Model_Entity_Pergunta();
            $this->entity->setDescricao($this->request->getParam('pergunta'));
            $this->entity->getFormulario()->setId($this->request->getParam('id'));
            //FUTURAMENTE DAR POSSIBILIDADE DE COLOCAR IMAGEM

            if ($this->request->getParam('tipo') == 'alternativa') {
                //ALTERNATIVA
                $this->entity->getTipopergunta()->setId(1);
                $dados = $this->request->getParam('alternativa');
            } else {
                //DISSERTATIVA
                $this->entity->getTipopergunta()->setId(2);
            }
            $this->dao = new Application_Model_Dao_Pergunta();
            $idPergunta = $this->dao->novaPergunta($this->entity);

            $this->dao = new Application_Model_Dao_Alternativa();
            $this->entity = new Application_Model_Entity_Alternativa();

            if ($this->request->getParam('tipo') == 'alternativa') {
                foreach ($dados as $row):
                    $this->entity->getPergunta()->setId($idPergunta);
                    $this->entity->setDescricao($row);
                    $this->dao->inserir($this->entity);
                endforeach;
            }
        }
        //Mostra tipos de pergunta
        $this->dao = new Application_Model_Dao_Tipopergunta();
        $this->view->tipoPergunta = $this->dao->buscaTipos();

        $this->dao = new Application_Model_Dao_Pergunta();
        $this->view->perguntas = $this->dao->buscarPorFormulario($this->request->getParam('id'));

        //BUSCA FORMULARIO POR ID
        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->formulario = $this->dao->buscarUmFormulario($this->request->getParam('id'));
        
        //PASSA ID FO FORMULARIO
        $this->view->id = $this->request->getParam('id');
    }

    public function detalheAction(){
        
        if ($this->getRequest()->isPost()) {

            
            $this->entity = new Application_Model_Entity_Pergunta();
            //$this->entity->setDescricao($this->request->getParam('pergunta'));
            //$this->entity->getFormulario()->setId($this->request->getParam('id'));
            //FUTURAMENTE DAR POSSIBILIDADE DE COLOCAR IMAGEM

            if ($this->request->getParam('tipo') == 'alternativa') {
                //ALTERNATIVA
                $this->entity->getTipopergunta()->setId(1);
                $dados = $this->request->getParam('alternativa');
            } else {
                //DISSERTATIVA
                $this->entity->getTipopergunta()->setId(2);
            }
            $this->dao = new Application_Model_Dao_Pergunta();
            $idPergunta = $this->dao->novaPergunta($this->entity);

            $this->dao = new Application_Model_Dao_Alternativa();
            $this->entity = new Application_Model_Entity_Alternativa();

            if ($this->request->getParam('tipo') == 'alternativa') {
                foreach ($dados as $row):
                    $this->entity->getPergunta()->setId($idPergunta);
                    $this->entity->setDescricao($row);
                    $this->dao->inserir($this->entity);
                endforeach;
            }
        }
        
        
        //Mostra tipos de pergunta
        $this->dao = new Application_Model_Dao_Tipopergunta();
        $this->view->tipoPergunta = $this->dao->buscaTipos();

        $this->dao = new Application_Model_Dao_Pergunta();
        $this->view->perguntas = $this->dao->buscarPorFormulario($this->request->getParam('id'));

        //BUSCA FORMULARIO POR ID
        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->formulario = $this->dao->buscarUmFormulario($this->request->getParam('id'));
        
        //PASSA ID FO FORMULARIO
        $this->view->id = $this->request->getParam('id');
    }
    
    public function pesquisaAction() {
        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->listaFormulario = $this->dao->buscarFormularios(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
    }
    
}