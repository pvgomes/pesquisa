<?php

/*
 * Controller referente a pesquisa e coisas duplicadas
 * 
 */

class PesquisaController extends Zend_Controller_Action {

    private $dao;
    private $entity;
    private $request;
    private $formulario;

    public function init() {
        $this->request = $this->getRequest();
    }

    public function formularioAction() {
        //CASO TENHA REQUISITAÇÃO NESSE MESMO ACTION
        if ($this->getRequest()->isPost()) {

            $this->formulario = new Application_Model_Entity_Formulario();
            $this->formulario->setNome($this->request->getParam('nome'));
            $this->formulario->setDescricao($this->request->getParam('descricao'));
            $this->formulario->getUsuario()->setId(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
            $this->formulario->getTema()->setId($this->request->getParam('tema'));

            $this->dao = new Application_Model_Dao_Formulario();
            $id = $this->dao->inserir($this->formulario);


            //verifica se ouve retorno
            if ($id != null) {
                $this->_redirect('/usuario/pergunta/id/' . $id);
                //$this->view->idform = $id;
            }
        }

        $this->dao = new Application_Model_Dao_Tema();
        $this->view->listaTema = $this->dao->buscaTema();

        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->modeloFormulario = $this->dao->buscaFormularioModelo();

        $this->view->meusModelos = $this->dao->buscarFormularios(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
    }

    public function novoformularioAction() {

        $id = $this->request->getParam('id');
        
        $this->formulario = new Application_Model_Entity_Formulario();
        $this->formulario->setNome($this->request->getParam('nome'));
        $this->formulario->setDescricao($this->request->getParam('descricao'));
        $this->formulario->getUsuario()->setId(Zend_Auth::getInstance()->getStorage()->read()->usu_id);
        $this->formulario->getTema()->setId($this->request->getParam('tema'));

        $this->dao = new Application_Model_Dao_Formulario();
                
        
        if($id == null){
            $idform = $this->dao->inserir($this->formulario);            
        }else{ 
            $idform = $this->dao->alterar($id, $this->formulario->getNome(),  $this->formulario->getDescricao());
        }

        //verifica se ouve retorno
        if ($idform != null) {
            $this->_redirect('/pesquisa/pergunta/id/' . $idform);
            //$this->view->idform = $id;
        }
    }

    public function indexAction() {
        
    }

    public function perguntaAction() {

        if ($this->getRequest()->isPost()) {
            
        }
        //Mostra tipos de pergunta
        $this->dao = new Application_Model_Dao_Tipopergunta();
        $this->view->tipoPergunta = $this->dao->buscaTipos();

        $this->dao = new Application_Model_Dao_Pergunta();
        $this->view->perguntas = $this->dao->buscarPorFormulario($this->request->getParam('id'));

        //BUSCA FORMULARIO POR ID
        $this->dao = new Application_Model_Dao_Formulario();
        $this->view->formulario = $this->dao->buscarUmFormulario($this->request->getParam('id'));

        //PASSA ID FO FORMULARIO
        $this->view->id = $this->request->getParam('id');
    }

    public function addperguntaAction() {
        $this->entity = new Application_Model_Entity_Pergunta();
        $this->entity->setDescricao($this->request->getParam('pergunta'));
        $this->entity->getFormulario()->setId($this->request->getParam('id'));
        //FUTURAMENTE DAR POSSIBILIDADE DE COLOCAR IMAGEM

        if ($this->request->getParam('tipo') == 'alternativa') {
            //ALTERNATIVA
            $this->entity->getTipopergunta()->setId(1);
            $dados = $this->request->getParam('alternativa');
        } else {
            //DISSERTATIVA
            $this->entity->getTipopergunta()->setId(2);
        }
        $this->dao = new Application_Model_Dao_Pergunta();
        $idPergunta = $this->dao->novaPergunta($this->entity);

        $this->dao = new Application_Model_Dao_Alternativa();
        $this->entity = new Application_Model_Entity_Alternativa();

        if ($this->request->getParam('tipo') == 'alternativa') {
            foreach ($dados as $row):
                $this->entity->getPergunta()->setId($idPergunta);
                $this->entity->setDescricao($row);
                $this->dao->inserir($this->entity);
            endforeach;
        }

        $this->_redirect('/pesquisa/pergunta/id/' . $this->request->getParam('id'));
    }

    public function delperguntaAction() {

        $id = $this->request->getParam('id');

        $this->dao = new Application_Model_Dao_Pergunta();
        $this->dao->delPergunta($id);
    }

    public function delalternativaAction() {
        $id = $this->request->getParam('id');
        $this->dao = new Application_Model_Dao_Alternativa();
        echo $this->dao->deletar($id);
        exit;
    }

    public function altalternativaAction() {
        $id = $this->request->getParam('id');
        $dado = $this->request->getParam('dado');
        $this->dao = new Application_Model_Dao_Alternativa();
        echo $this->dao->alterar($dado, $id);
        exit;
    }

    public function editperguntaAction() {

        $id = $this->request->getParam('id');


        $this->dao = new Application_Model_Dao_Pergunta();
        $re = $this->dao->buscaPerguntaAlternativa($id);

        var_dump($re);
        exit;
    }

    public function altperguntaAction() {
        $id = $this->request->getParam('id');
        $this->dao = new Application_Model_Dao_Pergunta();
        $this->view->pergunta = $this->dao->buscarPorFormulario($id, 1);
        $this->view->idform = $this->request->getParam('frm');
    }

    public function upperguntaAction() {

        $idform = $this->request->getParam('idformulario');

        $this->entity = new Application_Model_Entity_Pergunta();
        $this->entity->setDescricao($this->request->getParam('pergunta'));
        $this->entity->getTipopergunta()->setId(1);
        $this->entity->getFormulario()->setId($idform);
        $this->entity->setId($this->request->getParam('idpergunta'));

        $this->dao = new Application_Model_Dao_Pergunta();
        if ($this->dao->editPergunta($this->entity)) {
            $this->_redirect('/pesquisa/pergunta/id/' . $idform);
        } else {
            echo "ERRO AO ATUALIZAR";
            die;
        }
    }

    public function buscapesquisaAction() {
        $nome = $this->request->getParam('nome');

        $this->dao = new Application_Model_Dao_Pesquisa();
        $this->view->pesquisas = $this->dao->buscaPesquisaNome($nome, Zend_Auth::getInstance()->getStorage()->read()->usu_id);
    }

    public function deletarAction() {
        $id = $this->request->getParam('id');

        $this->dao = new Application_Model_Dao_Pesquisa();
        $this->dao->deletar($id);

        echo 1;
    }

    public function altformularioAction() {

        $id = $this->request->getParam('id');
        $tipo = $this->request->getParam('tipo');

        if ($tipo == "modelo") {
            $this->dao = new Application_Model_Dao_Formulario();
            $this->view->listaNome = $this->dao->modelo($id);
            
            $this->dao = new Application_Model_Dao_Tema();
            $this->view->listaTema = $this->dao->buscaTema();

            $this->dao = new Application_Model_Dao_Formulario();
            $this->view->listaNome = $this->dao->pegaNome($id);
                
        } else {
            $this->dao = new Application_Model_Dao_Tema();
            $this->view->listaTema = $this->dao->buscaTema();

            $this->dao = new Application_Model_Dao_Formulario();
            $this->view->listaNome = $this->dao->pegaNome($id);
        }
    }
    
    /**
     * Método que adiciona alternativa
     * @autor Paulo Victor 20/10/2011
     */
    public function addalternativaAction(){
        
        $this->entity = new Application_Model_Entity_Alternativa();
        $this->entity->setDescricao($this->request->getParam('dado'));
        $this->entity->getPergunta()->setId($this->request->getParam('id'));
        
        
        $this->dao = new Application_Model_Dao_Alternativa();
        echo $this->dao->inserir($this->entity);

        exit;
        
    }

}