<?php

class Application_Form_Cadastro extends Zend_Dojo_Form
{

    public function init()
    {
        $this->setName('frmCadastro');
		$this->setMethod('post');
		$nome = new Zend_Dojo_Form_Element_ValidationTextBox('nome');
		$nome->setLabel('Nome: ')->setRequired(true)->setRegExp("[\w\d]*");
                
                $email = new Zend_Dojo_Form_Element_ValidationTextBox('email');
                $email->setLabel('E-mail: ')->setRequired(true)->setRegExp("[\w\d]*");
                
                $usuario = new Zend_Dojo_Form_Element_ValidationTextBox('usuario');
                $usuario->setLabel('Usuario: ')->setRequired(true)->setRegExp("[\w\d]*");
                
		$password = new Zend_Dojo_Form_Element_PasswordTextBox('txtPassword');
		$password->setLabel('Senha: ')->setRequired(true)->setRegExp("[\d\w]*");
		

		$submit = new Zend_Dojo_Form_Element_SubmitButton('Cadastrar');

		$this->addElements(array($nome,$email,$usuario,$password,$submit));
    }


}

