<?php

require_once("Sam/Acl/Ini.php");
require_once("Sam/Auth/Plugin.php");

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    public function __construct($application) {
        parent::__construct($application);


        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        Zend_Registry::set('config', $config);

        $db = Zend_Db::factory($config->resources->db);
        Zend_Registry::set('db', $db);

        $acl_ini = APPLICATION_PATH . '/configs/roles.ini';
        $this->_acl = new Sam_Acl_Ini($acl_ini);
        Zend_Registry::set('zend_acl', $this->_acl);

        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Sam_Auth_Plugin($this->_acl));

        $this->_auth = Zend_Auth::getInstance();
        if ($this->_auth->hasIdentity()) {
            // yes ! we get his role
            $user = $this->_auth->getStorage()->read();
            $role = $user->tus_id;
        } else {
            // no = guest user
            $role = 'guest';
        }
        Zend_Registry::set('role', $role);
        $loader = Zend_Loader_Autoloader::getInstance();
        //$loader->registerNamespace('Modulos_');
        $loader->registerNamespace('Helper_');
    }

    protected function _initHelpers() {
        // Chamando Helpers para o controller
        Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH . '/controllers/helpers');
        
    }

}

