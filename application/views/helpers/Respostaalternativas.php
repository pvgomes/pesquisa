<?php

/**
 * Formatação de resposta de alternativas
 * Verifica se existe alternativa a exibir e se sim retorna elas formatadas
 * @author Paulo Victor Leite Lima Gomes
 * @see APPLICATION_PATH/views/helpers/Respostaalternativas
 */
class Zend_View_Helper_Respostaalternativas extends Zend_View_Helper_Abstract {

    /**
     * Método Principal
     * @param string $value Valor para Formatação
     * @param string $format Formato de Saída
     * @return string Valor Formatado
     */
    public function Respostaalternativas($alternativas,$pergunta) {

        $stralt = " ";

        if (count($alternativas) > 0) {
            
            $numero = 1;
            foreach ($alternativas as $i => $r):
                foreach ($r as $indice => $registro):
                    $stralt .= '<div class="span-18 push-3"><label><input name="pergunta['.$pergunta.']" type="radio" value="'.$indice.'" /> '. $this->convertN2L($numero) .') '.  $registro . '</label></div>';
                    $numero +=1;
                endforeach;
            endforeach;
            //$stralt.='<div class="span-18 push-3"><hr></div><br>';
        }else{
            $stralt = '<p><textarea name="dissertativa['.$pergunta.']"></textarea></p>';
        }

        return $stralt;
    }
    
    public function convertN2L($numero){
        return strtr($numero,"123456789", "abcdefghi");
    }

}