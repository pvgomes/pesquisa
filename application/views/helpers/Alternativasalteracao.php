<?php

/**
 * Formatação de alternativas
 * Verifica se existe alternativa a exibir e se sim retorna elas formatadas
 * @author Paulo Victor Leite Lima Gomes
 * @see APPLICATION_PATH/views/helpers/Alternativas
 */
class Zend_View_Helper_Alternativasalteracao extends Zend_View_Helper_Abstract {

    /**
     * Método Principal
     * @param string $value Valor para Formatação
     * @param string $format Formato de Saída
     * @return string Valor Formatado
     */
    public function Alternativasalteracao ($alternativas) {

        $stralt = " ";

        if (count($alternativas) > 0) {
            
            $numero = 1;
            foreach ($alternativas as $i => $r):
                foreach ($r as $indice => $registro):
                    $stralt .= '<div class="span-18 push-1" id="caixa'.$indice.'"><b>'. $this->convertN2L($numero) .') </b><input type="text" id="alt'.$indice.'" value="'.  $registro . '" size="30"> <label id="'.$indice.'" class="botao btnEditar" >Salvar</label> <label id="'.$indice.'" class="botao btnExcluir">Excluir</label> </div>';
                    $numero +=1;
                endforeach;
            endforeach;
            //$stralt.='<div class="span-18 push-3"><hr></div><br>';
        }

        return $stralt;
    }
    
    public function convertN2L($numero){
        return strtr($numero,"123456789", "abcdefghi");
    }

}