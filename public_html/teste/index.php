<?php 
/*
 * 
 */
class Pessoa{
    
    public $nome;
    public $idade;
    public $sexo;
    
    public function respirar(){
        echo "Respirando...";
    }
    
}

class Cliente extends Pessoa{
    
    public function saque($valor){
        return "Sacando ".$valor;
    }
}

$lucia = new Cliente();

$lucia->nome = "Lucia";
$lucia->saque();

echo $lucia->nome." esta ".$lucia->saque(200);

?>