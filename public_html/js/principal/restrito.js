$(document).ready(function(){
	
	content($('#restrito'));
	
	var chart;
	   chart = new Highcharts.Chart({
	      chart: {
	         renderTo: 'grafico',
	         defaultSeriesType: 'area'
	      },
	      title: {
	         text: 'Estado da pesquisa'
	      },
	      subtitle: {
	         text: ''
	      },
	      xAxis: {
	         categories: ['Jan', 'Fev', 'Março', 'Abril', 'Maio', 'Junho', 'Julho'],
	         tickmarkPlacement: 'off',
	         title: {
	            enabled: false
	         }
	      },
	      yAxis: {
	         title: {
	            text: 'Porcentagem'
	         }
	      },
	      tooltip: {
	         formatter: function() {
	                   return ''+
	                this.x +': '+ Highcharts.numberFormat(this.percentage, 1) +'% ('+
	                Highcharts.numberFormat(this.y, 0, ',') +' respostas)';
	         }

	      },
		  
		  		 credits: {

					enabled: false

				},
	      plotOptions: {
	         area: {
	            stacking: 'percent',
	            lineColor: '#ffffff',
	            lineWidth: 1,
	            marker: {
	               lineWidth: 1,
	               lineColor: '#ffffff'
	            }
	         }
	      },
	      series: [{
	         name: 'Respondidas',
	         data: [302, 635, 809, 947, 1402, 3634, 3268]
	      }, {
	         name: 'Finalizadas',
	         data: [106, 107, 111, 133, 221, 767, 3766]
	      }, {
	         name: 'Aguardando resposta',
	         data: [667, 
			 900, 
			 3,
			 408, 
			 547, 
			 729, 
			 628]
	      }]
	   });

	
});