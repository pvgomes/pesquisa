$(document).ready(function(){
    $("#cadusuario").validate({
        rules:{
            nome:{
                required:true
            },
            email:{
                required:true,
                email:true
            },
            usuario:{
                required:true,
                minlength:5
            },
            senha:{
                required:true,
                minlength:5
            },
            confirSenha:{
                required: true,
                equalTo: "#senha"
            }
        },
        messages:{
            nome:{
                required:"Nome requerido"
            },
            email:{
                required:"Email requerido",
                email:"Email inválido"
            },
            usuario:{
                required:"Usuario requerido",
                minlength:"Mínimo 5 caracteres"
            },
            senha:{
                required:"Senha requerida",
                minlength:"Mínimo de 5 caracteres"
            },
            confirSenha:{
                required: "Obrigatório",
                equalTo: "Não é igual"
            }
        },
        submitHandler:function(){

            $('#cadusuario').hide('slow');
            $('#carregando').show('slow');

		
            var dados = $('#cadusuario').serialize();

            var ajax = new CustomAjax();
            var retorno = ajax.simple(new Helper().baseUrl() + 'principal/cadastrar', dados);

            //alert(data);

            if(retorno == true){
				
                $('.formgrande input').each().val(''); 
                //$('#cadusuario').hide('slow');

                $('#cadastrook').show();
                
            }else{
                alert('OCORREU ALGUM ERRO NO CADASTRO');
            }		
			
        }
    });

    $(".dateBR").mask("99/99/9999");


    /**
     *TESTE DO E-MAIL
     */
    $('.email').blur(function(){
    	
        var email = trim($('.email').val());
    	
        if(email == ''){	
            //alert('E-mail nulo');
            return;
        }
    	
        $('#emailcarregando').show();
	
        var dados = $('.email').serialize();
	
        var ajax = new CustomAjax();
	
        var retorno = ajax.simple('/principal/verificaemail', dados);
	
        if(retorno == true){		
            //alert('E-MAIL LIBERADO');
            $('#emailerro').hide();
         
            $('#emailok').show();
            $('#emailcarregando').hide();
		
        }else{
            //alert('E-MAIL JÁ ESTA CADASTRADO NO SISTEMA');
            $('#emailerro').show();
            $('#emailok').hide();
            $('#emailcarregando').hide();
		
            $('.email').val('');
        }
	
    });


    /**
     *
     *TESTE DO USUARIO
     */
    $('.usuario').blur(function(){

        var usuario = trim($('.usuario').val());
    	
        if(usuario == ''){
            return;
        }
    	
        $('#usuariocarregando').show();
		
        var dados = $('.usuario').serialize();
		
        var ajax = new CustomAjax();
		
        var retorno = ajax.simple('/principal/verificausuario',dados);
		
        if(retorno == true){
				
            $('#usuariocarregando').hide();
            $('#usuariook').show();
            $('#usuarioerro').hide();
            /*
            $('#proibido').hide('fast');
            $('#livre').show('fast');
            */

        }else{
            
            $('#usuariocarregando').hide();
            $('#usuarioerro').show();
            $('#usuariook').hide();
            $('.usuario').val('');
            /*
            $('#livre').hide('fast');
            $('#proibido').show('fast');	
            */
        }
    });
	
    function trim(str) {
        return str.replace(/^\s+|\s+$/g,"");
    }
/*	
$('.botao').click(function(){
	

});	
*/	
});