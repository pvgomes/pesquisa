$('document').ready(function(){
    
    //PARA QUALQUER TEXTAREA POE VAL LIMPO
    $('.conteudo textarea').val('');
    
    content($('#restrito'));
    
    $.fx.speeds._default = 1000;
    $(function() {
        $( "#dialogpergunta" ).dialog({
            autoOpen: false,
            show: "blind",
            height: 580,
            width: 900,
            hide: "blind",
            modal: true
        });
        $("#btnNova").click(function(){
            $( "#dialogpergunta" ).dialog( "open" );
            return false;
        });
    });
    
    
    /*
     $("#btnNova").click(function(){
        
        var ajax = new CustomAjax();
        var retorno = ajax.single(new Helper().baseUrl() + 'pesquisa/excluirform/id/' + id);  
    });
    */

    $('#tipoPergunta').change(function(){
        var valor = $('#tipoPergunta').val();
        //Se o tipo da pergunta for dissertavita mostra DIV dissertativa caso contrário alternativa
   
        if(valor == 1){
            $('#caixaAlternativa').hide();
            $('#caixaDissertativa').show(1000);
            
        }else if(valor == 2){
            $('#caixaDissertativa').hide(1000);
            $('#caixaAlternativa').show();
            
        }else{
            $('#caixaAlternativa').hide();
            $('#caixaDissertativa').hide();
        }
    });
    
    var num = 0;
    
    $('#add').click(function(){
        num +=1;
        $('.caixaAlternativa').append('<span id="box'+ num +'"><input type="text" name="alternativa['+ num +']" value="" size="50"/> <img src="/imagens/error.png" onClick="delalt(this.id)" id="'+ num +'"></span>');	
        $('#valalt').val('1');
    });
    
    
    $('.delpergunta').click(function(){
        var id = this.id;
        //alert('ESSA PERGUNTA TEM \n O ID: '+ id); 
        var ajax = new CustomAjax();
        var retorno = ajax.single('/pesquisa/delpergunta/id/' + id);  
        
        if(retorno == 1){
            $('#per'+id).hide();
        }else{
            alert('ERRO AO EXCLUIR! \n tentar mais tarde.')
        }
       
    });
    
    /* CANCELADO EM 11/10/2011 PARA ALTERAÇÃO NORMAL
    $('.editpergunta').click(function(){
        var id = this.id;
        
        var ajax = new CustomAjax();
        var retorno = ajax.single('/pesquisa/editpergunta/id/' + id);
        
        alert(retorno);
    })
    */
    
    $('#perguntaAlternativa').validate({
        rules:{
            pergunta: {
                required: true
            },
            valalt:{
                required: true
            }
        },
        messages:{
            pergunta: {
                required: 'Obrigatório<br>'
            },
            valalt: {
                required: 'Pelo menos uma alternativa<br>'
            }
        }
        
    }); 
    
});

function delalt(id){
    //alert(id);
    //$('#box'+id).hide('slow');
    $('#box'+id).remove();
    
}