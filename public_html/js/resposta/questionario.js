/**
 * questionario.js
 * Retorna dados e validações para montagem da pesquisa
 * 1º validacao
 * 2º filtro
 * 3º responder
 */
$('document').ready(function(){

    /*
     * Informou senha
     */
    $("#btnInfosenha").click(function(){
        //COMPARA SENHA VIA AJAX 
        var id = $("#idpesquisa").val();
        var senha = $("#peq_senha").val();
        var ajax = new CustomAjax();
        var retorno = ajax.single(new Helper().baseUrl() + 'resposta/valida/idpesquisa/' + id + '/senha/' + senha); 


        if(retorno){
            //alert("PODE PASSAR");
            $('#validacao').hide('slow');
            $('#filtro').show('fast');
        }else{
            alert("SENHA ERRADA");
        }
    });
    
    /*
     * Sem senha
     */
    $("#btnIniciar").click(function(){
        var pes_nome = $("#pes_nome").val();
        //PRIMEIRO VERIFICAR SE pes_nome != null
        if(pes_nome == null || pes_nome == ''){
            alert('INFORME O SEU NOME ANTES DE COMEÇAR');
            return false;
        }
        
        //CARREGA FILTRO
        $('#validacao').hide('slow');
        $('#filtro').show('fast');
    });
    
    
    /*
     * Nivel 2 Filtro de perguntas
     * Usuario filtra pergntas que vai responder
     */
    $("#btnFiltro").click(function(){
        
        /**
         * Inserir um carregando pois pode ser demorado
         */
        $('#filtro').hide('slow');
        $('#carregandoperguntas').show('slow');
        
        var idformulario = $("#idformulario").val();
        var idpesquisa = $("#idpesquisa").val();
        var ajax = new CustomAjax();
        var retorno = ajax.single(new Helper().baseUrl() + 'resposta/responder/id/' + idformulario + '/idpesquisa/' + idpesquisa); 
        
        $("#responder").html('');
        $("#responder").html(retorno);
        
        $("#btnEnviarpesquisa").addClass('botao');
        /**
         * Tirar o carregando carregandoperguntas
         */
        $('#carregandoperguntas').hide('slow');
        $('#responder').show('fast');
    });
});

