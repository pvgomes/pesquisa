$('document').ready(function(){
  
    
    content($('#painel'));
    $(".dateBR").mask('99/99/9999');
    $('#dtinicio').removeClass('dateBR');
    $('#dtfim').removeClass('dateBR');
    
   
    $('#temdata').click(function(){
        //alert($('#temdata:checked').val());
    
        if($('#temdata:checked').val() == 'on'){
            $('#dtinicio').addClass('dateBR');
            $('#dtfim').addClass('dateBR');
            $('#infodata').show('slow');
            
            //VERIFICAR AQUI VALOR DAS DATAS
            $('#dtinicio').blur(function(){
                var data = $('#dtinicio').val();
                if(verificarData(data,4)){
                //$("#msgErro").hide();
                //alert('LIBERADO');
                }else{
                    //$("#msgErro").show();
                    //$("#InicioVigencia").val("");
                    alert("Data não pode ser retroativa");
                    $('#dtinicio').val('');
                }
            });
            
            $('#dtfim').blur(function(){
                var data = $('#dtfim').val();
                if(verificarData(data,4)){
                //$("#msgErro").hide();
                //alert('LIBERADO');
                }else{
                    //$("#msgErro").show();
                    //$("#InicioVigencia").val("");
                    alert("Data não pode ser retroativa");
                    $('#dtfim').val('');
                }
            });
            
        }else{
            $('#infodata').hide('slow');
            $('#dtinicio').removeClass('dateBR');
            $('#dtfim').removeClass('dateBR');
            $('#dtinicio').val('');
            $('#dtfim').val('');
        }
   
    });
    
    $('#pesquisa').validate({
        rules:{
            nome: {
                required: true
            },
            descricao:{
                required: true
            }
        },
        messages:{
            nome: {
                required: 'Informe o nome'
            },
            descricao: {
                required: 'Informe a descrição'
            }
        }
        
    });
    
    
    $('#acesso').click(function(){
        
        if($('#acesso').val() == 2){
            $('#gerarsenha').show('slow');
        }else{
            $('#gerarsenha').hide('slow');
        }
    });
    
    $('#btnGerar').click(function(){
        
        var ajax = new CustomAjax();
        var retorno = ajax.single('/usuario/gerarsenha');  
        
        $('#senha').val(retorno);
    });
});