$('document').ready(function(){
   
    content($('.painel'));
   
    /*
    * Criado botões para:
    * Editar formulario
    * Aplicar pesquisa
    * Ver pesquisa
    */
   
    $( ".editar" ).button({ 
        icons: {
            primary:'ui-icon-gear'
        } 
    });
    $( ".editarPergunta" ).button({ 
        icons: {
            primary:'ui-icon-gear'
        } 
    });
    $( ".ver" ).button({ 
        icons: {
            primary:'ui-icon-search'
        } 
    });
    $( ".aplicar" ).button({ 
        icons: {
            primary:'ui-icon-play'
        } 
    });
    $( ".deletar" ).button({ 
        icons: {
            primary:'ui-icon-trash'
        } 
    });
   
   
   
    //AÇÕES
    $(".deletar").click(function(){
        var id = this.id;
        
        var deletar = confirm("Deseja mesmo deletar? \n As pesquisas tambem serão deletadas");
        if (deletar){
            
            var ajax = new CustomAjax();
            var retorno = ajax.single('/usuario/excluirform/id/' + id);
            
            if(retorno == 1){
                $('#for'+id).hide('slow');
            }else{
                alert('ERRO PARA DELETAR');
            } 
        }
        else{
            alert("Não deletado");
            
        }
        
    });
    
    $("#pesquisa").click(function(){
        var valor= $("#pesquisainput").val();
       
        var ajax = new CustomAjax();
        var retorno = ajax.single('/pesquisa/buscapesquisa/nome/'+ valor);
       
        if(retorno){
            $("#bloco").html("");
            $("#bloco").html(retorno);
        }else{
            $("#bloco").html("Erro na consulta");
        }
    });
    
    $(".ver").click(function(){
        var id = this.id;
       
        var ajax = new CustomAjax();
        var retorno = ajax.single('/usuario/verformulario/id/' + id)
       
        if(retorno){
            $("#peq" + id).show();
            $("#fechar" + id).show();
            $("#peq" + id).html("");
            $("#peq" + id).html(retorno);
        }else{
            $("#peq" + id).html("Erro na consulta");
        }
       
    });
    
    $(".fechar").click(function(){
       var id = this.id;
       
       $("#peq" + id ).hide('slow');
       $("#fechar" + id ).hide('slow');
    });
   
});