$(document).ready(function(){
    $.fx.speeds._default = 1000;
    
    $( ".editar" ).button({ 
        icons: {
            primary:'ui-icon-gear'
        } 
    });
    $( ".editarPergunta" ).button({ 
        icons: {
            primary:'ui-icon-gear'
        } 
    });
    $( ".ver" ).button({ 
        icons: {
            primary:'ui-icon-search'
        } 
    });
    $( ".aplicar" ).button({ 
        icons: {
            primary:'ui-icon-play'
        } 
    });
    $( ".deletar" ).button({ 
        icons: {
            primary:'ui-icon-trash'
        } 
    });
    
    $(function() {
        $( "#dialogalterar" ).dialog({
            autoOpen: false,
            show: "blind",
            height: 580,
            width: 1000,
            hide: "blind"
        });
        //QUANDO CLICAR NO BOTAO EDITAR DA PAGINA ABRE O DIALOG
        $( ".editar" ).click(function() {
            $( "#dialogalterar" ).dialog( "open" );
            //pega o id que esta no botao editar
            var id = this.id;
            
            //passa para a alteração o nome antigo do form
            $("#nome").val($("#nomevelho"+id).val());
            $("#idParaAlteracao").val(id);
        });
        //QUANDO CLICAR NO BOTAO DE ALTERAR DO DIALOG
        $("#alterar").click(function(){
            var id  = $("#idParaAlteracao").val();
            var nome= $("#nome").val();
            
            var ajax = new CustomAjax();
            var dados = $("#novonome").serialize();
            var retorno = ajax.simple(new Helper().baseUrl() + 'admin/editarform/id/' + id + '/nome/' + nome, dados)
            
            alert('admin/editarform/id/' + id + '/nome/' + nome);
            
            if(retorno == true){
                alert('aaaaaa');
            }else{
                alert('OCORREU ALGUM ERRO NO CADASTRO');
            }
        })
    });
    
    
    $(".botao").button();	
	
    $('#btnNovo').click(function(){	
        $("#informacoes").hide();
		
        $("#novoform").fadeIn("slow");
		
        $('#btnInserir').click(function(){

            // $("#divPergunta").html('');			
            //  $('#cadpergunta').show();

            });
		
    });
	
    $('#btnVolta').click(function(){
        //limpando o formulario antigo
        $("#novoform").hide();
        $("#informacoes").fadeIn("slow");
    });
	
 
    $('#cadformulario').formerize();
    $(".botao").button();
	
    $('.excluir').click(function(){
        var id = this.id;
           
        var deletar = confirm("Deseja mesmo deletar? \n O modelo de formulario irá sumir.")
        if (deletar){

            var ajax = new CustomAjax();
            var retorno = ajax.single(new Helper().baseUrl() + 'admin/excluirform/id/' + id);
            
            if(retorno == 1){
                $('#form'+id).hide('slow');
            }else{
                alert('ERRO PARA DELETAR');
            } 
        }
        else{
            alert("Não deletaa")
        }
           
       
    });
    
/*$('.editar').click(function(){
        var id = this.id;
        var nome = this.nome;

        var ajax = new CustomAjax();
        var retorno = ajax.single(new Helper().baseUrl() + 'admin/editarform/id/' + id + '/nome/' + nome);
            
        if(retorno == 1){
            $('#form'+id).hide('slow');
        }else{
            alert('ERRO PARA EDITAR');
        }
    });*/
});