$(document).ready(function(){
    $.fx.speeds._default = 1000;
    $(function() {
        $( "#dialogplano" ).dialog({
            autoOpen: false,
            show: "blind",
            height: 580,
            width: 900,
            hide: "blind"
        });
        $("#dialogsenha").dialog({
            autoOpen: false,
            show: "blind",
            height: 580,
            width: 900,
            hide: "blind"
        });
        $( "#plano" ).click(function() {
            $( "#dialogplano" ).dialog( "open" );
            return false;
        });
        $("#alterarsenha").click(function(){
            $( "#dialogsenha" ).dialog( "open" );
            return false;
        });
    });
    
    $('#novasenha').validate({
        rules:{
            senhaantiga:{
                required:true,
                equalTo:"#senhaoculta"
            },
            senhanova:{
                required:true
            }
        },
        message:{
            senhaantiga:{
                required:"Senha antiga é obrigatória",
                equalTo:"Senha antiga não está certa"
            },
            senhanova:{
                required:"Senha nova é obrigatória"
            }
        },
        submitHandler:function(){
            $('#novasenha').attr('action', '/admin/alterarsenha');
            var ajax = new CustomAjax();
            var retorno = ajax.slack('#novasenha');
           
            
            if(retorno == true){
                $('.formgrande input').each().val(''); 
                $('#novasenha').hide();

                $('#index').show(2000);
            }else{
                alert('OCORREU ALGUM ERRO NO CADASTRO');
            }
        }
    
   
    });
   
    
});
