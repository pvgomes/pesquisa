$('document').ready(function(){
    content($('#restrito'));
    
    $.fx.speeds._default = 1000;
    $(function() {
        $( "#dialogpergunta" ).dialog({
            autoOpen: false,
            show: "blind",
            height: 580,
            width: 900,
            hide: "blind",
            modal: true
        });
        $( "#dialogalterarpergunta" ).dialog({
            autoOpen: false,
            show: "blind",
            height: 580,
            width: 900,
            hide: "blind",
            modal: true
        });
        $("#btnNova").click(function(){
            $( "#dialogpergunta" ).dialog( "open" );
            return false;
        });
        $("#editar").click(function(){
            $( "#dialogalterarpergunta" ).dialog( "open" );
            return false;
        });
        $("#editar").click(function(){
            var id = this.value;
            alert(id);
        })
    });
    
    
    /*
     $("#btnNova").click(function(){
        
        var ajax = new CustomAjax();
        var retorno = ajax.single(new Helper().baseUrl() + 'pesquisa/excluirform/id/' + id);  
    });
    */

    $('#tipoPerguntaAlt').change(function(){
        var valor = $('#tipoPerguntaAlt').val();
          
        alternativa(valor, $('#caixaDissertativaAlt'), $('#caixaAlternativaAlt'));

    });
    
    $('#tipoPergunta').change(function(){
        var valor = $('#tipoPergunta').val();
   
        alternativa(valor, $('#caixaDissertativa'), $('#caixaAlternativa'));

    });
    
    var num = 0;
    
    $('#add').click(function(){
        num +=1;
        $('.caixaAlternativa').append('<span id="box'+ num +'"><input type="text" name="alternativa['+ num +']" value="" size="50"/> <img src="/imagens/error.png" onClick="delalt(this.id)" id="'+ num +'"></span>');	
        
   
    });
    
    
    $('.delpergunta').click(function(){
        var id = this.id;
        alert('ESSA PERGUNTA TEM \n O ID: '+ id); 
    });
});

function delalt(id){
    //alert(id);
    //$('#box'+id).hide('slow');
    $('#box'+id).remove();
    
}

//FUNCAO QUE PEGA OS PARAMETROS E MOSTRA E ESCONDE OS INPUTS DAS ALTERNATIVAS
function alternativa(tipoPergunta, dissertativa, alternativa){
    //Se o tipo da pergunta for dissertavita mostra DIV dissertativa caso contrário alternativa
    if(tipoPergunta == 1){
        dissertativa.show(1000);
        alternativa.hide();
            
    }else if(tipoPergunta == 2){
        dissertativa.hide();
        alternativa.show(1000);           
    }else{
        dissertativa.hide();
        alternativa.hide();
    }
}